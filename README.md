# Transformed-Based Myoelectric Decoding for Continuous Control of Prosthetic Fingers
This repository accompanies my MScR thesis in the context of the [CDT in Biomedical AI](https://web.inf.ed.ac.uk/cdt/biomedical-ai) at the [University of Edinburgh](https://www.ed.ac.uk/).  

Supervisors: [Prof. Dr Kia Nazarpour](https://kianazarpour.github.io/) & [Dr. Chenfei Ma](https://uk.linkedin.com/in/chenfei-ma)

Contact information:
| Name | Email address | Website |
| :--- | :--- | :--- |
| Wolf De Wulf | [wolf.de.wulf@ed.ac.uk](mailto:wolf.de.wulf@ed.ac.uk) | https://www.wolfdewulf.eu |

## Usage

### 1. Creating and activating the environment

Install [Python 3.10.8](https://www.python.org/downloads/release/python-3108/) and use [virtualenv](https://docs.python.org/3/library/venv.html) to create a new environment:
```bash
python -m venv env
```

Then, activate that environment:

```bash
source env/bin/activate
```

To deactivate the virtual environment, use:

```bash
deactivate
```

Install the required packages using:
```bash
pip install -r requirements.txt
```

### 2. Data

Run the [`download.py`](data/download.py) script to download the data:

```bash
python data/download.py --help
```
The raw data is downloaded to `data/raw`.

Run the [`process.py`](data/process.py) script to process the data using a certain [configuration file](configs/):

```bash
python data/process.py --help
```

### 3. Reproducing empirical evaluations
The empirical evaluations can be reproduced using the [`train.py`](scripts/train.py) script with the same [configuration file](configs/):
```bash
python scripts/train.py --help
```

### 4. Questions
I am happy to answer questions or to provide the results/predictions/models, please send me an email!
