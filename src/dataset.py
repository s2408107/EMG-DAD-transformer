import os

import numpy
import sklearn
import torch


class EMGDataset(torch.utils.data.Dataset):
    def __init__(
        self,
        storage,
        participant,
        dataset_dir,
        dataset_processed,
        num_participants,
        amputees,
        num_repetitions,
        num_runs,
        num_gestures,
        num_digits,
        num_digit_actions,
        num_sensors,
        amputee_num_sensors,
        emg_new_sfreq,
        envelope_patch_window,
        envelope_mulaw,
        glove_filter_order,
        running_window_size,
        window_size,
        window_stride,
        tolerance,
        tolerance_limit,
        glove_map,
        training_split,
        validation_split,
        testing_split,
        weight_classes,
        features,
        **kwargs,
    ):
        self.storage = storage
        self.participant = participant
        self.dataset_dir = os.path.join(
            storage,
            dataset_dir,
            dataset_processed,
            f"participant_{participant}",
            f"exercise_1",
        )
        self.features = features
        self.num_participants = num_participants
        self.amputees = amputees
        self.num_repetitions = num_repetitions
        self.num_runs = num_runs
        self.num_gestures = num_gestures
        self.num_digits = num_digits
        self.num_digit_actions = num_digit_actions
        self.num_sensors = amputee_num_sensors.get(self.participant, num_sensors)
        self.envelope_patch_window = envelope_patch_window
        self.envelope_mulaw = envelope_mulaw
        self.glove_filter_order = glove_filter_order
        self.window_size = window_size if running_window_size is None else running_window_size
        self.window_size_in_samples = int((self.window_size / 1000) * emg_new_sfreq)
        self.window_stride = window_stride
        self.window_stride_in_samples = int((self.window_stride / 1000) * emg_new_sfreq)
        self.tolerance = tolerance
        self.tolerance_limit = tolerance_limit
        self.glove_map = glove_map
        self.training_split = training_split
        self.validation_split = validation_split
        self.testing_split = testing_split
        self.weight_classes = weight_classes
        self.weights = {}

        torch.multiprocessing.set_sharing_strategy("file_system")

        # Load X
        self.X = []
        for run in range(1, 1 + self.num_runs):
            if self.features:
                features = []
                for feature in [
                    "stfs",
                    "ar",
                    "logvar",
                    "waveform_length",
                    "wilson_amplitude",
                    "kurtosis",
                    "skewness",
                    "hjorth_activity",
                    "hjorth_complexity",
                    "hjorth_mobility",
                ]:
                    features.append(
                        numpy.load(
                            os.path.join(
                                self.dataset_dir,
                                f"run_{run}",
                                f"{feature}.npy",
                            )
                        )
                    )
                data = numpy.concatenate(features, axis=-1)
            else:
                data = numpy.load(
                    os.path.join(
                        self.dataset_dir,
                        f"run_{run}",
                        f"envelope_{'mulaw' if self.envelope_mulaw else 'norm'}.npy",
                    )
                )
            self.X.append(data)
        if self.features:
            self.num_features = self.X[0].shape[2]
        else:
            self.num_features = 0

        # Load y
        self.y = []
        for run in range(1, 1 + self.num_runs):
            self.y += [
                (
                    numpy.load(
                        os.path.join(
                            self.dataset_dir,
                            f"run_{run}",
                            "digit_action_labels.npy",
                        ),
                    ),
                    numpy.load(
                        os.path.join(
                            self.dataset_dir,
                            f"run_{run}",
                            "stimulus.npy",
                        ),
                    ),
                )
            ]

        # Load repetitions
        self.repetitions = []
        for run in range(1, 1 + self.num_runs):
            self.repetitions += [
                numpy.load(
                    os.path.join(
                        self.dataset_dir,
                        f"run_{run}",
                        "repetition.npy",
                    ),
                )
            ]

    def create_dataloader(self, batch_size, split_config, calculate_weights=False, shuffle=True):
        idxs = []
        for run, repetitions in zip(split_config["runs"], split_config["repetitions"]):
            repetition_idxs = numpy.where(numpy.isin(self.repetitions[run - 1], repetitions))[0]
            if calculate_weights:
                self.weights["da"] = [
                    sklearn.utils.class_weight.compute_class_weight(
                        class_weight="balanced",
                        classes=range(self.num_digit_actions),
                        y=self.y[run - 1][0][repetition_idxs][:, da],
                    )
                    for da in range(self.num_digits)
                ]
                self.weights["stim"] = [
                    sklearn.utils.class_weight.compute_class_weight(
                        class_weight="balanced",
                        classes=range(self.num_gestures),
                        y=self.y[run - 1][1][repetition_idxs],
                    )
                ]
            idxs.append((run - 1, repetition_idxs))

        # Cross product
        cross = numpy.concatenate(
            [numpy.vstack([numpy.full(len(idxs), run), idxs]).T for run, idxs in idxs]
        )

        return torch.utils.data.DataLoader(
            self,
            sampler=torch.utils.data.sampler.SubsetRandomSampler(cross.tolist())
            if shuffle
            else cross.tolist(),
            batch_size=len(cross) - 1 if batch_size is None else batch_size,
            drop_last=True,
        )

    def get_dataloaders(self, batch_size=None):
        return dict(
            training=self.create_dataloader(
                batch_size, self.training_split, calculate_weights=True, shuffle=True
            ),
            validation=self.create_dataloader(
                batch_size, self.validation_split, calculate_weights=False, shuffle=False
            ),
            testing=self.create_dataloader(
                batch_size, self.testing_split, calculate_weights=False, shuffle=False
            ),
        )

    def __len__(self):
        return len(self.y)

    def __getitem__(self, idx):
        run_idx, window_idx = idx
        if self.features:
            X = self.X[run_idx][window_idx]
        else:
            window_start_idx = self.window_stride_in_samples * window_idx
            window_end_idx = window_start_idx + self.window_size_in_samples
            X = self.X[run_idx][window_start_idx:window_end_idx]
        return (
            X,
            {
                "da": self.y[run_idx][0][window_idx],
                "stim": torch.tensor([self.y[run_idx][1][window_idx]]),
            },
        )
