from typing import Dict, List, Optional

from pydantic import BaseModel, conlist, validator


class Split(BaseModel):
    runs: List[int]
    repetitions: List[List[int]]


class DatasetConfig(BaseModel):
    dataset_name: str
    dataset_dir: str
    dataset_processed: str
    num_participants: int
    amputees: Optional[List[int]] = []
    num_exercises: int
    num_repetitions: int
    num_runs: int
    num_gestures: int
    gestures: List[str] = [
        "rest",
        "thumb flexion/extension",
        "thumb abduction/adduction",
        "index flexion/extension",
        "middle flexion/extension",
        "ring&little flexion/extension",
        "index point",
        "cylindrical grip",
        "lateral grip",
        "tripod grip",
    ]
    num_digits: int
    digits: List[str] = [
        "Thumb rotation",
        "Thumb flexion",
        "Index flexion",
        "Middle flexion",
        "Ring flexion",
        "Little flexion",
    ]
    num_digit_actions: int
    digit_actions: List = ["open", "stall", "close"]
    num_sensors: int
    amputee_num_sensors: Optional[Dict[int, int]] = {}
    emg_sfreq: float
    emg_new_sfreq: float
    envelope_mulaw: bool
    envelope_highfreq: float
    envelope_filter_orders: List[int]
    envelope_patch_window: Optional[int] = None
    features_lowfreq: float
    features_highfreq: float
    features_filter_order: int
    mu: float
    glove_sfreq: float
    glove_highfreq: float
    glove_filter_order: int
    window_size: int
    running_window_size: Optional[int]
    window_stride: int
    tolerance: float
    tolerance_limit: float
    glove_map: Optional[dict]
    training_split: Split
    validation_split: Split
    testing_split: Optional[Split]
    weight_classes: bool


class PretrainedModelInfo(BaseModel):
    path_to_config: str
    path_to_model: str


class PretrainedFULLInfo(BaseModel):
    TNET: PretrainedModelInfo
    FNET: PretrainedModelInfo


class Hyperparams(BaseModel):
    # Model
    num_layers: Optional[int]
    activation_function: Optional[str]
    input_dim: Optional[int]
    num_heads: Optional[int]
    dropout: Optional[float]
    filter_orders: Optional[List[int]]
    # Training
    batch_size: int
    learning_rate: float
    weight_decay: float


class ModelConfig(BaseModel):
    net: str
    pretrained_weights: Optional[PretrainedFULLInfo]
    output_type: str
    single_token: bool
    single_head: bool

    @validator("single_head", always=True)
    def validate_output_tokens(cls, single_head, values):
        if single_head and values.get("single_token"):
            if not values.get("outputs") == "stim":
                raise ValueError(
                    "Can only combine single output head with single output token when the outputs are single movements."
                )
        return single_head


class TrainingConfig(BaseModel):
    random_seed: int
    hpo_metric: List[str]
    hpo_metric_mode: Optional[List[str]]
    hpo_samples: Optional[int]
    hpo_parallel: Optional[int]
    hpo_pp: Optional[bool]


# Combination class
class Config(BaseModel):
    dataset_config: DatasetConfig
    hyperparams: Optional[List[Hyperparams]]
    model_config: ModelConfig
    training_config: TrainingConfig

    @validator("hyperparams", always=True)
    def validate_hyperparams(cls, hyperparams, values):
        if (
            hyperparams is not None
            and len(hyperparams) != 1
            and len(hyperparams) != values.get("dataset_config").num_participants
        ):
            raise ValueError(
                "If individual hyperparameters are used, there must be a set for each of them!."
            )
        return hyperparams
