import gc
import imp
import os
import random
import time
from datetime import datetime
from typing import Optional, Tuple

import flatdict
import numpy
import torch
import yaml


def start_timer():
    gc.collect()
    torch.cuda.empty_cache()
    torch.cuda.reset_peak_memory_stats()
    torch.cuda.synchronize()
    return time.time()


def end_timer_and_print(start_time):
    torch.cuda.synchronize()
    end_time = time.time()
    print("Total execution time = {:.3f} sec".format(end_time - start_time))
    print(
        "Max memory used by tensors = {} bytes".format(
            torch.cuda.max_memory_allocated()
        )
    )


def model_class(model_name, cwd=os.getcwd()):
    return getattr(
        imp.load_source(
            model_name, os.path.join(cwd, "src", "models", f"{model_name}.py")
        ),
        model_name,
    )


def setup_experiment_folder(outputs_dir: str) -> Tuple[str, str]:
    """
    Utility function to create and setup the experiment output directory.
    Return both output and checkpoint directories.
    Args:
        outputs_dir (str): The parent directory to store
            all outputs across experiments.
    Returns:
        Tuple[str, str]:
            outputs_dir: Directory of the outputs (checkpoint_dir and logs)
            checkpoint_dir: Directory of the training checkpoints
    """
    now = datetime.now().strftime("%Y_%m_%d__%H_%M_%S")
    outputs_dir = os.path.join(outputs_dir, now)
    checkpoint_dir = os.path.join(outputs_dir, "checkpoint")
    os.makedirs(checkpoint_dir, exist_ok=True)

    return outputs_dir, checkpoint_dir


def setup_device(device: Optional[str] = None) -> torch.device:
    """
    Utility function to setup the device for training.
    Set to the selected CUDA device(s) if exist.
    Set to "mps" if using Apple silicon chip.
    Set to "cpu" for others.
    Args:
        device (Optional[str], optional): Integer of the GPU device id,
            or str of comma-separated device ids. Defaults to None.
    Returns:
        torch.device: The chosen device(s) for the training
    """
    if torch.cuda.is_available():
        device = f"cuda:{device}"
    else:
        try:
            if torch.backends.mps.is_available():
                device = "mps"
        except:
            device = "cpu"
    return torch.device(device)


def setup_random_seed(seed: int, is_deterministic: bool = True) -> None:
    """
    Utility function to setup random seed. Apply this function early on the training script.
    Args:
        seed (int): Integer indicating the desired seed.
        is_deterministic (bool, optional): Set deterministic flag of CUDNN. Defaults to True.
    """
    # set the seeds
    random.seed(seed)
    numpy.random.seed(seed)
    torch.manual_seed(seed)
    if torch.cuda.is_available():
        torch.cuda.manual_seed(seed)
        torch.cuda.manual_seed_all(seed)
        if is_deterministic is True:
            torch.backends.cudnn.deterministic = True
            torch.backends.cudnn.benchmark = False


def count_parameters(model: torch.nn.Module) -> int:
    """
    Utility function to calculate the number of parameters in a model.
    Args:
        model (nn.Module): Model in question.
    Returns:
        int: Number of parameters of the model in question.
    """
    return sum(p.numel() for p in model.parameters() if p.requires_grad)


def load_yaml(filepath: str) -> dict:
    """
    Utility function to load yaml file, mainly for config files.
    Args:
        filepath (str): Path to the config file.
    Raises:
        exc: Stop process if there is a problem when loading the file.
    Returns:
        dict: Training configs.
    """
    with open(filepath, "r") as stream:
        try:
            return yaml.safe_load(stream)
        except yaml.YAMLError as exc:
            raise exc


def create_config_file(config, hpo_result, metrics):
    config.hyperparams = hpo_result.metrics.config
    config.metrics = {metric: hpo_result.metrics[metric] for metric in metrics}
    return config


def list_of_nested_dicts_to_nested_dict(list):
    if type(list[0]) != dict:
        return list
    else:
        return {
            k: list_of_nested_dicts_to_nested_dict([dic[k] for dic in list])
            for k in list[0]
        }


def calculate_statistic(statistic, participants_results):
    combined_dict = list_of_nested_dicts_to_nested_dict(participants_results)
    flat_dict = flatdict.FlatDict(combined_dict)
    statistics = [
        [*key.split(":"), float(getattr(numpy, statistic)(value))]
        for key, value in flat_dict.items()
    ]

    separated_dict = {}

    def insert(dct, lst):
        for x in lst[:-2]:
            dct[x] = dct = dct.get(x, dict())
        dct.update({lst[-2]: lst[-1]})

    for statistic in statistics:
        insert(separated_dict, statistic)
    return separated_dict
