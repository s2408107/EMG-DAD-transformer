import os

import torch


class EMGTransformer(torch.nn.Module):
    def __init__(
        self,
        participant,
        num_digits,
        num_digit_actions,
        num_gestures,
        pretrained_weights,
        num_layers,
        input_dim,
        activation_function,
        num_heads,
        dropout,
        net,
        output_type,
        single_head,
        single_token,
        num_sensors,
        window_size_in_samples,
        envelope_patch_window,
        **kwargs,
    ):
        super().__init__()

        self.participant = participant
        self.num_digits = num_digits
        self.num_digit_actions = num_digit_actions
        self.num_gestures = num_gestures
        self.pretrained_weights = pretrained_weights
        self.num_layers = num_layers
        self.input_dim = input_dim
        self.activation_function = activation_function
        self.num_heads = num_heads
        self.ff_dim = 4 * input_dim
        self.dropout = dropout
        self.net = net
        self.single_head = single_head
        self.single_token = single_token
        self.output_type = output_type
        self.num_sensors = num_sensors
        self.window_size_in_samples = window_size_in_samples
        self.envelope_patch_window = envelope_patch_window
        self.output_heads = torch.nn.ModuleDict()
        self.num_output_tokens = 1 if (single_token or output_type == "stim") else num_digits
        getattr(self, f"setup_{net}")()

    def setup_output(self, prefix):
        if self.output_type == "da":
            for head in range(1 if self.single_head else self.num_digits):
                self.output_heads[f"{prefix}_da{head}"] = torch.nn.Linear(
                    self.input_dim, self.num_digit_actions
                )
        elif self.output_type == "stim":
            self.output_heads[f"{prefix}_stim"] = torch.nn.Linear(
                self.input_dim, self.num_gestures
            )

    def setup_tnet(self):
        self.tnet_token_dim = self.window_size_in_samples
        self.tnet_seq_length = self.num_sensors

        # linear input layer
        self.tnet_linear_input_layer = torch.nn.Linear(self.tnet_token_dim, self.input_dim)

        # positional embedding
        self.tnet_positional_embedding = torch.nn.Parameter(
            torch.zeros(self.tnet_seq_length, self.input_dim)
        )
        torch.nn.init.xavier_uniform_(self.tnet_positional_embedding)

        # encoder
        self.tnet_encoder = torch.nn.TransformerEncoder(
            torch.nn.TransformerEncoderLayer(
                self.input_dim,
                self.num_heads,
                self.ff_dim,
                self.dropout,
                self.activation_function,
                batch_first=True,
                norm_first=True,
            ),
            self.num_layers,
        )

        # output heads
        self.setup_output("tnet")

    def setup_fnet(self):
        self.fnet_token_dim = self.envelope_patch_window * self.num_sensors
        self.fnet_seq_length = int(self.window_size_in_samples / self.envelope_patch_window)

        # linear input layer
        self.fnet_linear_input_layer = torch.nn.Linear(self.fnet_token_dim, self.input_dim)

        # positional embedding
        self.fnet_positional_embedding = torch.nn.Parameter(
            torch.zeros(self.fnet_seq_length, self.input_dim)
        )
        torch.nn.init.xavier_uniform_(self.fnet_positional_embedding)

        # encoder
        self.fnet_encoder = torch.nn.TransformerEncoder(
            torch.nn.TransformerEncoderLayer(
                self.input_dim,
                self.num_heads,
                self.ff_dim,
                self.dropout,
                self.activation_function,
                batch_first=True,
                norm_first=True,
            ),
            self.num_layers,
        )

        # output heads
        self.setup_output("fnet")

    def setup_joint(self):
        self.setup_tnet()
        self.setup_fnet()
        self.setup_output("joint")

    def setup_frozen_joint(self):
        # Load pretrained tnet
        self.setup_tnet()
        self.load_state_dict(
            torch.load(
                os.path.join(
                    "models", self.pretrained_weights["tnet"], f"participant_{self.participant}.pt"
                )
            )
        )

        # Load pretrained fnet
        self.setup_fnet()
        self.load_state_dict(
            torch.load(
                os.path.join(
                    "models", self.pretrained_weights["fnet"], f"participant_{self.participant}.pt"
                )
            )
        )

        # Setup final layers
        self.setup_output("joint")

    def forward_tnet(self, x):
        x = x.swapaxes(1, 2).reshape(len(x), self.num_sensors, -1)
        x = self.tnet_linear_input_layer(x)
        x = x + self.tnet_positional_embedding
        x = self.tnet_encoder(x)
        return x[:, : self.num_output_tokens]

    def forward_fnet(self, x):
        x = x.reshape(len(x), self.window_size_in_samples, -1)
        x = x.unfold(dimension=1, size=self.envelope_patch_window, step=self.envelope_patch_window)
        x = x.reshape(len(x), self.fnet_seq_length, -1)
        x = self.fnet_linear_input_layer(x)
        x = x + self.fnet_positional_embedding
        x = self.fnet_encoder(x)
        return x[:, : self.num_output_tokens]

    def forward_output_da(self, tokens, net):
        if self.single_head:
            return self.output_heads[f"{net}_da0"](tokens)
        else:
            if self.single_token:
                return torch.stack(
                    [
                        self.output_heads[f"{net}_da{head}"](tokens)
                        for head in range(self.num_digits)
                    ],
                    dim=1,
                )
            else:
                return torch.stack(
                    [
                        self.output_heads[f"{net}_da{head}"](token)
                        for token, head in zip(tokens.swapaxes(0, 1), list(range(self.num_digits)))
                    ],
                    dim=1,
                )

    def forward_output_stim(self, tokens, net):
        return self.output_heads[f"{net}_stim"](tokens[:, 0].reshape(len(tokens), -1))[:, None]

    def forward(self, x):
        if self.net == "tnet" or self.net == "joint":
            tnet_tokens = self.forward_tnet(x)
            tnet_outputs = getattr(self, f"forward_output_{self.output_type}")(tnet_tokens, "tnet")
            if self.net == "tnet":
                return {"tnet": tnet_outputs}
        if self.net == "fnet" or self.net == "joint":
            fnet_tokens = self.forward_fnet(x)
            fnet_outputs = getattr(self, f"forward_output_{self.output_type}")(fnet_tokens, "fnet")
            if self.net == "fnet":
                return {"fnet": fnet_outputs}
        if self.net == "joint":
            joint_tokens = tnet_tokens + fnet_tokens
            joint_outputs = getattr(self, f"forward_output_{self.output_type}")(
                joint_tokens, "joint"
            )
            return {"tnet": tnet_outputs, "fnet": fnet_outputs, "joint": joint_outputs}
