import torch


def forward_output_da(self, tokens):
    if self.single_head:
        return self.output_heads[f"output_0"](tokens)
    else:
        if self.single_token:
            return torch.stack(
                [self.output_heads[f"output_{head}"](tokens) for head in range(self.num_digits)],
                dim=1,
            )
        else:
            return torch.stack(
                [
                    self.output_heads[f"output_{head}"](token)
                    for token, head in zip(tokens.swapaxes(0, 1), list(range(self.num_digits)))
                ],
                dim=1,
            )


def forward_output_stim(self, tokens):
    return self.output_heads[f"output_stim"](tokens[:, 0].reshape(len(tokens), -1))[:, None]
