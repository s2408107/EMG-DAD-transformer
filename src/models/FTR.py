import torch

import src.models.utils


class FTR(torch.nn.Module):
    def __init__(
        self,
        num_digits,
        num_digit_actions,
        num_gestures,
        num_layers,
        activation_function,
        dropout,
        net,
        output_type,
        single_head,
        single_token,
        num_sensors,
        num_features,
        **kwargs,
    ):
        super().__init__()

        self.num_digits = num_digits
        self.num_digit_actions = num_digit_actions
        self.num_gestures = num_gestures
        self.num_layers = num_layers
        self.activation_function = activation_function
        self.dropout = dropout
        self.single_head = single_head
        self.net = net
        self.output_type = output_type
        self.num_sensors = num_sensors
        self.input_dim = num_features
        self.ff_dim = 4 * num_features
        self.num_output_tokens = (
            1 if (single_token or output_type == "stim") else num_digits
        )
        self.output_heads = torch.nn.ModuleDict()
        self.input_shape = (self.num_sensors, self.input_dim)

        self.setup()

    def setup(self):
        # positional embedding
        self.positional_embedding = torch.nn.Parameter(
            torch.zeros(self.num_sensors, self.input_dim)
        )
        torch.nn.init.xavier_uniform_(self.positional_embedding)

        # encoder
        self.encoder = torch.nn.TransformerEncoder(
            torch.nn.TransformerEncoderLayer(
                d_model=self.input_dim,
                nhead=1,
                dim_feedforward=self.ff_dim,
                activation=self.activation_function,
                dropout=self.dropout,
                batch_first=True,
                norm_first=True,
            ),
            self.num_layers,
        )

        # output heads
        if self.output_type == "da":
            for head in range(1 if self.single_head else self.num_digits):
                self.output_heads[f"output_{head}"] = torch.nn.Linear(
                    self.input_dim, self.num_digit_actions
                )
        elif self.output_type == "stim":
            self.output_heads[f"output_stim"] = torch.nn.Linear(
                self.input_dim, self.num_gestures
            )

    def forward(self, x):
        x = x + self.positional_embedding
        x = self.encoder(x)
        x = x[:, : self.num_output_tokens]
        return x, dict(
            FTR=getattr(
                src.models.utils, f"forward_output_{self.output_type}"
            )(self, x)
        )

    def __str__(self):
        return "FTR"
