import os

import torch

import src.models.utils


class TNET(torch.nn.Module):
    def __init__(
        self,
        participant,
        num_digits,
        num_digit_actions,
        num_gestures,
        pretrained_weights,
        num_layers,
        input_dim,
        activation_function,
        num_heads,
        dropout,
        output_type,
        single_head,
        single_token,
        num_sensors,
        window_size_in_samples,
        new_input_layer=False,
        storage=os.getcwd(),
        **kwargs,
    ):
        super().__init__()

        self.num_digits = num_digits
        self.num_digit_actions = num_digit_actions
        self.num_gestures = num_gestures
        self.num_layers = num_layers if num_layers is not None else 1
        self.input_dim = input_dim
        self.activation_function = activation_function
        self.num_heads = num_heads
        self.ff_dim = 4 * input_dim
        self.dropout = dropout if dropout is not None else 0
        self.single_head = single_head
        self.single_token = single_token
        self.output_type = output_type
        self.num_sensors = num_sensors
        self.window_size_in_samples = window_size_in_samples
        self.new_input_layer = new_input_layer
        self.output_heads = torch.nn.ModuleDict()
        self.num_output_tokens = (
            1 if (single_token or output_type == "stim") else num_digits
        )
        self.input_shape = (self.num_sensors, self.window_size_in_samples)

        self.setup()
        if pretrained_weights is not None:
            pretrained_weights = torch.load(
                os.path.join(
                    storage,
                    pretrained_weights,
                    f"participant_{participant}.pt",
                )
            )
            if new_input_layer:
                pretrained_weights.pop("linear_input_layer.weight")
                pretrained_weights.pop("linear_input_layer.bias")
            self.load_state_dict(pretrained_weights, strict=False)

    def setup(self):
        self.token_dim = self.window_size_in_samples
        self.seq_length = self.num_sensors

        # linear input layer
        self.linear_input_layer = torch.nn.Linear(
            self.token_dim, self.input_dim
        )

        # positional embedding
        self.positional_embedding = torch.nn.Parameter(
            torch.zeros(self.seq_length, self.input_dim)
        )
        torch.nn.init.xavier_uniform_(self.positional_embedding)

        # encoder
        self.encoder = torch.nn.TransformerEncoder(
            torch.nn.TransformerEncoderLayer(
                d_model=self.input_dim,
                nhead=self.num_heads,
                dim_feedforward=self.ff_dim,
                activation=self.activation_function,
                dropout=self.dropout,
                batch_first=True,
                norm_first=True,
            ),
            self.num_layers,
        )

        # output heads
        if self.output_type == "da":
            for head in range(1 if self.single_head else self.num_digits):
                self.output_heads[f"output_{head}"] = torch.nn.Linear(
                    self.input_dim, self.num_digit_actions
                )
        elif self.output_type == "stim":
            self.output_heads[f"output_stim"] = torch.nn.Linear(
                self.input_dim, self.num_gestures
            )

    def freeze(self):
        for name, param in list(self.named_parameters()):
            if (
                name
                in [
                    "linear_input_layer.weight",
                    "linear_input_layer.bias",
                ]
                and self.new_input_layer
            ):
                param.requires_grad = True
            else:
                param.requires_grad = False

    def forward(self, x):
        x = x.swapaxes(1, 2).reshape(len(x), self.num_sensors, -1)
        x = self.linear_input_layer(x)
        x = x + self.positional_embedding
        x = self.encoder(x)
        x = x[:, : self.num_output_tokens]
        return x, dict(
            TNET=getattr(
                src.models.utils, f"forward_output_{self.output_type}"
            )(self, x)
        )

    def str(self):
        return "TNET"
