import os

import torch

import src.models.utils
import src.utils
from src.config import Config
from src.models.FNET import FNET
from src.models.TNET import TNET


class FULL(torch.nn.Module):
    def __init__(
        self,
        pretrained_weights,
        **kwargs,
    ):
        super().__init__()
        for attribute, value in kwargs.items():
            setattr(self, attribute, value)
        self.input_shape = (self.num_sensors, self.window_size_in_samples)
        self.output_heads = torch.nn.ModuleDict()
        self.num_output_tokens = (
            1 if (self.single_token or self.output_type == "stim") else self.num_digits
        )

        kwargs_TNET = kwargs.copy()
        kwargs_FNET = kwargs.copy()
        self.pretrained = pretrained_weights is not None
        if self.pretrained:
            # TNET config
            TNET_config = Config(
                **src.utils.load_yaml(
                    os.path.join(self.global_cwd, pretrained_weights["TNET"]["path_to_config"])
                )
            )
            # overwrite hyperparams
            for hyperparam, value in TNET_config.hyperparams[self.participant - 1].dict().items():
                kwargs_TNET[hyperparam] = value
            # overwrite model config
            for model_param, value in TNET_config.model_config.dict().items():
                kwargs_TNET[model_param] = value
            # set pretraining
            kwargs_TNET["pretrained_weights"] = pretrained_weights["TNET"]["path_to_model"]
            # check if new input layer is needed
            kwargs_TNET["new_input_layer"] = (
                int(
                    (TNET_config.dataset_config.window_size / 1000)
                    * TNET_config.dataset_config.emg_new_sfreq
                )
                != kwargs["window_size_in_samples"]
            )

            # FNET config
            FNET_config = Config(
                **src.utils.load_yaml(
                    os.path.join(self.global_cwd, pretrained_weights["FNET"]["path_to_config"])
                )
            )
            # overwrite hyperparams
            for hyperparam, value in FNET_config.hyperparams[self.participant - 1].dict().items():
                kwargs_FNET[hyperparam] = value
            # overwrite model config
            for model_param, value in FNET_config.model_config.dict().items():
                kwargs_FNET[model_param] = value
            # set pretraining
            kwargs_FNET["pretrained_weights"] = pretrained_weights["FNET"]["path_to_model"]
            # check if new input layer is needed
            kwargs_FNET["new_input_layer"] = (
                int(
                    (FNET_config.dataset_config.window_size / 1000)
                    * FNET_config.dataset_config.emg_new_sfreq
                )
                != kwargs["window_size_in_samples"]
            )
        else:
            kwargs_TNET["pretrained_weights"] = None
            kwargs_FNET["pretrained_weights"] = None

        # Make submodels
        self.TNET = TNET(
            **kwargs_TNET,
        )
        self.FNET = FNET(
            **kwargs_FNET,
        )

        # Freeze
        if self.pretrained:
            self.TNET.freeze()
            self.FNET.freeze()

        # output heads
        joint_dim = (
            self.TNET.positional_embedding.shape[-1] + self.FNET.positional_embedding.shape[-1]
        )
        if self.output_type == "da":
            for head in range(1 if self.single_head else self.num_digits):
                self.output_heads[f"output_{head}"] = torch.nn.Sequential(
                    torch.nn.Linear(joint_dim, joint_dim),
                    torch.nn.Linear(joint_dim, self.num_digit_actions),
                )
        elif self.output_type == "stim":
            self.output_heads[f"output_stim"] = torch.nn.Sequential(
                torch.nn.Linear(joint_dim, joint_dim),
                torch.nn.Linear(joint_dim, self.num_gestures),
            )

    def forward(self, x):
        tnet_tokens, tnet_outputs = self.TNET(x)
        fnet_tokens, fnet_outputs = self.FNET(x)
        joint_tokens = torch.concatenate([tnet_tokens, fnet_tokens], axis=-1)
        return joint_tokens, dict(
            TNET=tnet_outputs["TNET"],
            FNET=fnet_outputs["FNET"],
            FULL=getattr(src.models.utils, f"forward_output_{self.output_type}")(
                self, joint_tokens
            ),
        )

    def str(self):
        return "FULL"
