import numpy
import scipy


def window(data, window_size, window_stride, sfreq):
    return numpy.lib.stride_tricks.sliding_window_view(
        data,
        int(
            (window_size / 1000) * sfreq
        ),  # window size in samples = (window_size (ms) / 1000) * sfreq (Hz)
        axis=0,
    )[
        :: int(
            (window_stride / 1000) * sfreq
        ),  # window stride in samples = (window_stride (ms) / 1000) * sfreq (Hz)
        :,
        :,
    ]


def compute_digit_action_labels(data, num_digits, tolerance, tolerance_limit):
    # Estimate joint velocities using 1st order diffs
    velocities = numpy.diff(data, axis=0)
    velocities = numpy.vstack((numpy.zeros((1, num_digits)), velocities))  # pad first row

    # Threshold to get action labels
    digit_action_labels = numpy.zeros_like(data, dtype=int)
    digit_action_labels[velocities > 0] = 2
    digit_action_labels[velocities < 0] = 0
    digit_action_labels[numpy.abs(velocities) < tolerance] = 1  # tolerance
    digit_action_labels[data > 1.0 - tolerance_limit] = 2  # tolerance at boundaries
    digit_action_labels[data < tolerance_limit] = 0

    return digit_action_labels


def butterworth_filter(data, sfreq, highfreq, order, type, filter):
    b, a = scipy.signal.butter(
        order,
        highfreq,
        fs=sfreq,
        btype=type,
        analog=False,
    )
    return getattr(scipy.signal, filter)(
        b,
        a,
        data,
        axis=0,
        # padtype="odd",
        # padlen=100,
        method="gust",
    )


def savgol_filter(data, sfreq, highfreq, order, type, filter):
    return scipy.signal.savgol_filter(data, axis=0, mode="interp", window_length=2000, polyorder=2)


def gaussian_filter(data, sfreq, highfreq, order, type, filter):
    return scipy.ndimage.gaussian_filter1d(data, sigma=1, axis=0, mode="reflect")


def iir_filter(data, sfreq, highfreq, order, type, filter):
    b, a = scipy.signal.iirfilter(
        order, highfreq, btype=type, analog=False, ftype="butter", output="ba", fs=sfreq
    )
    return getattr(scipy.signal, filter)(b, a, data, axis=0, method="gust")


def butterworth_filter_band(data, sfreq, lowfreq, highfreq, order):
    nyq = 0.5 * sfreq
    low = lowfreq / nyq
    high = highfreq / nyq
    b, a = scipy.signal.butter(order, [low, high], btype="band")
    return scipy.signal.filtfilt(
        b=b,
        a=a,
        x=data,
        axis=2,
        method="pad",
        padtype="odd",
        padlen=numpy.minimum(3 * numpy.maximum(len(a), len(b)), data.shape[2] - 1),
    )


def comb_filter(windows):
    """Comb filtering at 50 Hz, for 2KHz sampling frequency.
    Coefficients are computed with MATLAB."""
    b = numpy.zeros(41)
    a = numpy.zeros(41)
    b[0] = 0.941160767899653
    b[-1] = -0.941160767899653
    a[0] = 1.0
    a[-1] = -0.882321535799305
    return scipy.signal.filtfilt(
        b=b,
        a=a,
        x=windows,
        axis=2,
        method="pad",
        padtype="odd",
        padlen=numpy.minimum(3 * numpy.maximum(len(a), len(b)), windows.shape[2] - 1),
    )


def waveform_length(x, axis=-1, keepdims=False):
    """Computes the waveform length (WL) of each signal.
    Waveform length is the sum of the absolute value of the deltas between
    adjacent values (in time) of the signal:
    .. math:: \\text{WL} = \\sum_{i=1}^{N-1} | x_{i+1} - x_i |
    Parameters
    ----------
    x : ndarray
        Input data. Use the ``axis`` argument to specify the "time axis".
    axis : int, optional
        The axis to compute the feature along. By default, it is computed along
        rows, so the input is assumed to be shape (n_channels, n_samples).
    keepdims : bool, optional
        Whether or not to keep the dimensionality of the input. That is, if the
        input is 2D, the output will be 2D even if a dimension collapses to
        size 1.
    Returns
    -------
    y : ndarray, shape (n_channels,)
        WL of each channel.
    References
    ----------
    .. [1] B. Hudgins, P. Parker, and R. N. Scott, "A New Strategy for
       Multifunction Myoelectric Control," IEEE Transactions on Biomedical
       Engineering, vol. 40, no. 1, pp. 82-94, 1993.
    """
    return numpy.expand_dims(
        numpy.sum(numpy.absolute(numpy.diff(x, axis=axis)), axis=axis, keepdims=keepdims), axis=-1
    )


def logvar(x, axis=-1, keepdims=False):
    """Log of the variance of the signal.
    .. math::
        \\text{log-var} = \\log \left( \\frac{1}{N}
            \\sum_{i=1}^{N} \\left(x_i - \\mu \\right)^2 \\right)
    For electrophysiological signals that are mean-zero, this is the log of the
    mean square value, making it similar to :func:`root_mean_square` but
    scaling differently (slower) with :math:`x`.
    For EMG data recorded from forearm muscles, log-var has been found to
    relate to wrist angle fairly linearly [1]_.
    Note: base-10 logarithm is used, though the base is not specified in [1]_.
    Parameters
    ----------
    x : ndarray
        Input data. Use the ``axis`` argument to specify the "time axis".
    axis : int, optional
        The axis to compute the feature along. By default, it is computed along
        rows, so the input is assumed to be shape (n_channels, n_samples).
    keepdims : bool, optional
        Whether or not to keep the dimensionality of the input. That is, if the
        input is 2D, the output will be 2D even if a dimension collapses to
        size 1.
    Returns
    -------
    y : ndarray, shape (n_channels,)
        log-var of each channel.
    References
    ----------
    .. [1] J. M. Hahne, F. Bießmann, N. Jiang, H. Rehbaum, D. Farina, F. C.
       Meinecke, K.-R. Müller, and L. C. Parra, "Linear and Nonlinear
       Regression Techniques for Simultaneous and Proportional Myoelectric
       Control," IEEE Transactions on Neural Systems and Rehabilitation
       Engineering, vol. 22, no. 2, pp. 269–279, 2014.
    """
    return numpy.expand_dims(numpy.log10(numpy.var(x, axis=axis, keepdims=keepdims)), axis=-1)


def wilson_amplitude(x, axis=-1, keepdims=False):
    return numpy.expand_dims(
        numpy.sum(
            numpy.abs(numpy.diff(x, n=1, axis=axis)) > 5e-6,
            axis=axis,
            keepdims=keepdims,
        ),
        axis=-1,
    )


def ar(x, axis=-1, keepdims=False):
    from statsmodels.tsa.ar_model import AutoReg

    windows = []
    for window in x:
        channels = []
        for channel in window:
            channels.append(numpy.array(AutoReg(channel, lags=4).fit().params))
        windows.append(numpy.stack(channels))
    return numpy.stack(windows)


def hjorth_activity(x, axis=-1):
    return numpy.expand_dims(numpy.var(x, axis=axis), axis=-1)


def hjorth_mobility(x, axis=-1):
    activity = [numpy.var(x_, axis=axis) for x_ in [numpy.diff(x, i, axis=axis) for i in range(3)]]
    mobility = [numpy.sqrt(activity[i + 1] / activity[i]) for i in range(2)]
    return numpy.expand_dims(mobility[0], axis=-1)


def hjorth_complexity(x, axis=-1):
    activity = [numpy.var(x_, axis=axis) for x_ in [numpy.diff(x, i, axis=axis) for i in range(3)]]
    mobility = [numpy.sqrt(activity[i + 1] / activity[i]) for i in range(2)]
    complexity = mobility[1] / mobility[0]
    return numpy.expand_dims(complexity, axis=-1)


def skewness(x, axis=-1):
    return numpy.expand_dims(scipy.stats.skew(x, axis=axis), axis=-1)


def kurtosis(x, axis=-1):
    return numpy.expand_dims(scipy.stats.kurtosis(x, axis=axis), axis=-1)


def stfs(x, axis=-1):
    x = x.swapaxes(0, -1)
    num_sensors = x.shape[1]
    centred = x - x.mean(axis=0)

    SSI = numpy.power(centred, 2).sum(axis=0)
    SSI_1 = numpy.power(SSI, 0.1)
    SSI_f = SSI_1 / 0.1

    DerHOM_1 = numpy.diff(centred, axis=0)
    DerHOM_2 = numpy.diff(DerHOM_1, axis=0)

    abs_DerHOM_1 = abs(DerHOM_1)
    log_abs_DerHOM_1 = numpy.log(abs_DerHOM_1)
    # mDiff1 = log_abs_DerHOM_1.mean(axis=0)
    # mDiff1_f = numpy.power(mDiff1, 0.1) * 10
    abs_DerHOM_2 = abs(DerHOM_2)
    mDiff2_f = abs_DerHOM_2.mean(axis=0)

    LDK = abs(centred)
    MLK = LDK.mean(axis=0)

    power_DerHOM_1 = numpy.power(DerHOM_1, 2)
    sum_DerHOM_1 = power_DerHOM_1.sum(axis=0)
    D_sum_DerHOM_1 = sum_DerHOM_1 / (num_sensors - 1)
    NRS_1 = numpy.sqrt(D_sum_DerHOM_1)
    NRS_1_f = numpy.power(NRS_1, 0.1) * 10

    power_DerHOM_2 = numpy.power(DerHOM_2, 2)
    sum_DerHOM_2 = power_DerHOM_2.sum(axis=0)
    D_sum_DerHOM_2 = sum_DerHOM_2 / (num_sensors - 1)
    NRS_2 = numpy.sqrt(D_sum_DerHOM_2)
    NRS_2_f = numpy.power(NRS_2, 0.1) * 10

    expo = numpy.array(
        [
            0.75,
            0.75,
            0.75,
            0.75,
            0.75,
            0.75,
            0.75,
            0.75,
            0.75,
            0.75,
            0.75,
            0.75,
            0.75,
            0.75,
            0.75,
            0.75,
        ]
    )

    for index, item in enumerate(expo):
        if index <= 0.75 * 16 and index >= 0.25 * 16:
            expo[index] = 0.5

    # expo_curwin = numpy.power(centred, expo, dtype=complex)
    # mean_expo_curwin = expo_curwin.mean(axis=0)
    # ASM = abs(mean_expo_curwin)

    power_curwin = numpy.power(centred, 0.5, dtype=complex)
    sum_power_curwin = power_curwin.sum(axis=0)
    MSR = sum_power_curwin / num_sensors

    SSI_NSR_1 = SSI_f - NRS_1_f
    SSI_NSR_2 = SSI_f - NRS_2_f

    return abs(numpy.stack((SSI_f, SSI_NSR_1, SSI_NSR_2, MLK, mDiff2_f, MSR), axis=-1)).swapaxes(
        0, 1
    )
