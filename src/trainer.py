import gc
import logging
import os

import torch
import torch._dynamo
import torchinfo
import tqdm
import yaml

import src.utils as utils
import wandb
from src.dataset import EMGDataset
from src.models.FULL import FULL


class Trainer:
    def __init__(
        self,
        evaluator,
        device,
        config,
        log_to_wandb,
        storage,
        optimising,
        mixed_precision,
        compile,
        global_cwd=os.getcwd(),
        **kwargs,
    ):
        self.evaluator = evaluator
        self.device = device
        self.config = config
        self.log_to_wandb = log_to_wandb
        self.storage = storage
        self.optimising = optimising
        self.mixed_precision = mixed_precision
        self.compile = compile
        self.global_cwd = global_cwd
        torch.set_float32_matmul_precision("high")

    def setup_loss_fn(self, model, dataset):
        self.loss_fns = {}
        nets = (
            ["TNET", "FNET", "FULL"]
            if (
                model.str() == "FULL" and (not model.pretrained)
            )  # or model.TNET.new_input_layer))
            else [model.str()]
        )
        for net in nets:
            self.loss_fns[net] = []
            for output in (
                range(1)
                if model.output_type == "stim"
                else range(dataset.num_digits)
            ):
                self.loss_fns[net].append(
                    torch.nn.CrossEntropyLoss(
                        weight=torch.tensor(
                            dataset.weights[model.output_type][output]
                        ).to(self.device, dtype=torch.float)
                    )
                    if self.config.dataset_config.weight_classes
                    else torch.nn.CrossEntropyLoss()
                )

        def multitask_loss(predictions, trues):
            return sum(
                [
                    loss_fn(prediction, true.to(self.device, dtype=torch.long))
                    for net in nets
                    for loss_fn, prediction, true in zip(
                        self.loss_fns[net],
                        predictions[net].swapaxes(0, 1),
                        trues[model.output_type].swapaxes(0, 1),
                    )
                ]
            )

        return multitask_loss

    def setup_optimiser(self, model, learning_rate, weight_decay):
        return torch.optim.Adam(
            model.parameters(),
            lr=learning_rate,
            weight_decay=weight_decay,
        )

    def train_epoch(
        self, epoch, model, loss_fn, optimiser, scaler, dataloader
    ):
        # Set the model to training mode
        model.train()

        # Go through data once
        total_loss = 0.0
        total_examples = 0
        with tqdm.tqdm(
            enumerate(dataloader),
            desc=f" * Epoch {epoch}, batch",
            unit="",
            total=len(dataloader),
            disable=self.optimising,
        ) as tepoch:
            for batch_idx, batch in tepoch:
                optimiser.zero_grad(set_to_none=True)

                with torch.autocast(
                    device_type=str(self.device),
                    dtype=torch.bfloat16,
                    enabled=self.mixed_precision,
                ):
                    # Get batch
                    inputs = batch[0].to(self.device, dtype=torch.float32)

                    # Get predictions
                    outputs = model(inputs)[1]

                    # Compute loss
                    loss = loss_fn(outputs, batch[1])

                # Compute (scaled) loss gradients
                scaler.scale(loss).backward()

                # Do backprop
                scaler.step(optimiser)
                scaler.update()

                # Record epoch loss
                num_examples = inputs.size(0)
                total_loss += loss.item() * num_examples
                total_examples += num_examples
                if not self.optimising:
                    tepoch.set_postfix(loss=total_loss / total_examples)

        return total_loss / total_examples

    def train_participant(self, participant, experiment_name):
        if not self.optimising:
            print("-" * 25 + f"\n- Participant {participant}:")

        # Set hyperparams to specific or common hyperparams
        hyperparams = self.config.hyperparams[
            0 if len(self.config.hyperparams) == 1 else participant - 1
        ]

        # Initialise W&B
        run = wandb.init(
            project="EMG-DAD-transformer",
            group=experiment_name,
            name=f"partcipant {participant}",
            config=self.config.dict(),
            reinit=True,
            mode="online" if self.log_to_wandb else "disabled",
            dir=self.storage,
        )

        # Create dataset
        dataset = EMGDataset(
            self.storage,
            participant,
            features=self.config.model_config.net == "FTR",
            **self.config.dataset_config.dict(),
        )
        loaders = dataset.get_dataloaders(hyperparams.batch_size)

        # Create model
        model_args = dict(
            participant=participant,
            num_digits=dataset.num_digits,
            num_digit_actions=dataset.num_digit_actions,
            num_gestures=dataset.num_gestures,
            num_sensors=dataset.num_sensors,
            window_size_in_samples=dataset.window_size_in_samples,
            envelope_patch_window=dataset.envelope_patch_window,
            num_features=dataset.num_features,
            global_cwd=self.global_cwd,
            storage=self.storage,
        )
        model = utils.model_class(
            self.config.model_config.net, self.global_cwd
        )(
            **model_args,
            **hyperparams.dict(),
            **self.config.model_config.dict(),
        )
        if self.compile:
            torch._dynamo.config.log_level = logging.ERROR
            model = torch.compile(model)

        model.to(device=self.device)
        wandb.watch(model)

        # Create loss function
        loss_fn = self.setup_loss_fn(model, dataset)

        # Create optimiser
        optimiser = self.setup_optimiser(
            model, hyperparams.learning_rate, hyperparams.weight_decay
        )

        # Create gradient scaler (mixed precision)
        scaler = torch.cuda.amp.GradScaler(enabled=self.mixed_precision)

        # Loop over the dataset multiple times
        results = {}
        best_validation_results = 0
        best_model = model.state_dict()
        best_model_epoch = 1
        for epoch in range(1, 41):
            # Train a single epoch
            loss = self.train_epoch(
                epoch, model, loss_fn, optimiser, scaler, loaders["training"]
            )

            # Evaluate
            model.eval()
            with torch.no_grad():
                results["training"] = self.evaluator.evaluate(
                    model,
                    loaders["training"],
                )
                results["validation"] = self.evaluator.evaluate(
                    model,
                    loaders["validation"],
                )

            validation_results = results["validation"]
            current_validation_metric = validation_results[model.output_type][
                self.config.training_config.hpo_metric[0]
            ]["macro_average"]
            best_validation_metric = (
                best_validation_results[model.output_type][
                    self.config.training_config.hpo_metric[0]
                ]["macro_average"]
                if best_validation_results != 0
                else 0
            )
            if current_validation_metric > best_validation_metric:
                best_validation_results = validation_results
                best_model = model.state_dict()
                best_model_epoch = epoch

            # Log
            if not self.optimising:
                print(
                    yaml.dump(
                        results, allow_unicode=True, default_flow_style=False
                    )
                )
                wandb.log(dict(results, loss=loss))

        # Store best validation results
        results["validation"] = best_validation_results

        # Calculate test metrics using best model
        if not self.optimising:
            model.load_state_dict(best_model)
            model.eval()
            with torch.no_grad():
                results["testing"] = self.evaluator.evaluate(
                    model, loaders["testing"], log_cm=True
                )
            print(
                yaml.dump(
                    dict(
                        model_epoch=best_model_epoch,
                        testing=results["testing"],
                    ),
                    allow_unicode=True,
                    default_flow_style=False,
                )
            )
            wandb.log(dict(testing=results["testing"]))

        # End W&B run
        run.finish()

        return (
            results,
            dict(
                model=model,
                info=torchinfo.summary(
                    model,
                    [hyperparams.batch_size] + list(model.input_shape),
                    verbose=0,
                ),
            ),
            loaders,
        )

    def train(self, experiment_name):
        # Loop over participants
        participants_results = []
        models = []
        for participant in range(
            1, 1 + self.config.dataset_config.num_participants
        ):
            # Train and store results
            results, model, _ = self.train_participant(
                participant, experiment_name
            )
            participants_results.append(results)
            models.append(model)

            # Clean up
            gc.collect()

        # Calculate mean/median/... over participants
        results = {
            statistic: utils.calculate_statistic(
                statistic, participants_results
            )
            for statistic in ["mean", "median"]
        }

        return results, models
