import numpy
import sklearn.discriminant_analysis
import sklearn.metrics
import torch

import wandb


class Evaluator:
    def __init__(self, device, gestures, digits, digit_actions):
        self.device = device
        self.gestures = gestures
        self.digits = digits
        self.digit_actions = digit_actions
        self.metrics = {
            "stim": ["accuracy_score"],
            "da": [
                "accuracy_score",
                "hamming_score",
                "f1_score",
                "recall",
                "precision",
            ],
        }
        self.fancy_names = {
            metric: metric.replace("_", " ").title() for metric in self.metrics["da"]
        }

    def evaluate(self, model, loader, log_cm=False):
        metrics = {}
        predictions = []
        trues_da = []
        trues_stim = []
        for batch_X, batch_y in loader:
            predicted = model(batch_X.to(self.device, dtype=torch.float32))[1]
            predictions.append(
                torch.argmax(
                    predicted[model.str()].softmax(dim=-1),
                    -1,
                )
                .cpu()
                .numpy()
            )
            trues_da.append(batch_y["da"])
            trues_stim.append(batch_y["stim"])

        predictions = numpy.concatenate(predictions)
        trues_da = numpy.concatenate(trues_da)
        trues_stim = numpy.concatenate(trues_stim)

        # Get indices per movement
        movement_idxs = {
            movement: numpy.where(trues_stim == movement)[0]
            for movement in range(len(self.gestures))
        }

        # Calculate metrics
        metrics = {
            model.output_type: {
                metric: getattr(self, metric)(
                    predictions, trues_da, trues_stim, model.output_type, movement_idxs
                )
                for metric in self.metrics[model.output_type]
            }
        }

        if log_cm:
            self.log_confusion_matrix(
                predictions,
                trues_da if model.output_type == "da" else trues_stim,
                model.output_type,
            )
        return metrics

    def evaluate_baseline_da_stall(self, loaders):
        _, y_training = next(iter(loaders["training"]))
        _, y_validation = next(iter(loaders["validation"]))
        _, y_testing = next(iter(loaders["testing"]))

        # Get indices per movement
        movement_idxs_training = {
            movement: numpy.where(y_training["stim"] == movement)[0]
            for movement in range(len(self.gestures))
        }
        movement_idxs_validation = {
            movement: numpy.where(y_validation["stim"] == movement)[0]
            for movement in range(len(self.gestures))
        }
        movement_idxs_testing = {
            movement: numpy.where(y_testing["stim"] == movement)[0]
            for movement in range(len(self.gestures))
        }

        # Extract da from y
        y_training = y_training["da"]
        y_validation = y_validation["da"]
        y_testing = y_testing["da"]

        predictions_training = numpy.ones(y_training.shape)
        predictions_validation = numpy.ones(y_validation.shape)
        predictions_testing = numpy.ones(y_testing.shape)

        self.log_confusion_matrix(predictions_testing, y_testing, "da")
        return {
            split: {
                "da": {
                    metric: getattr(self, metric)(
                        torch.tensor(predictions),
                        trues,
                        None,
                        "da",
                        movement_idxs,
                    )
                    for metric in self.metrics["da"]
                }
            }
            for split, trues, predictions, movement_idxs in zip(
                ["training", "validation", "testing"],
                [y_training, y_validation, y_testing],
                [predictions_training, predictions_validation, predictions_testing],
                [movement_idxs_training, movement_idxs_validation, movement_idxs_testing],
            )
        }

    def evaluate_baseline_da_LDA(self, loaders):
        X_training, y_training = next(iter(loaders["training"]))
        X_validation, y_validation = next(iter(loaders["validation"]))
        X_testing, y_testing = next(iter(loaders["testing"]))

        # Reshape X
        X_training = X_training.reshape(len(X_training), -1)
        X_validation = X_validation.reshape(len(X_validation), -1)
        X_testing = X_testing.reshape(len(X_testing), -1)

        # Get indices per movement
        movement_idxs_training = {
            movement: numpy.where(y_training["stim"] == movement)[0]
            for movement in range(len(self.gestures))
        }
        movement_idxs_validation = {
            movement: numpy.where(y_validation["stim"] == movement)[0]
            for movement in range(len(self.gestures))
        }
        movement_idxs_testing = {
            movement: numpy.where(y_testing["stim"] == movement)[0]
            for movement in range(len(self.gestures))
        }

        # Extract da from y
        y_training = y_training["da"]
        y_validation = y_validation["da"]
        y_testing = y_testing["da"]

        predictions_training = []
        predictions_validation = []
        predictions_testing = []
        for digit in range(len(self.digits)):
            lda = sklearn.discriminant_analysis.LinearDiscriminantAnalysis()
            lda.fit(X_training, y_training[:, digit])
            predictions_training.append(lda.predict(X_training))
            predictions_validation.append(lda.predict(X_validation))
            predictions_testing.append(lda.predict(X_testing))

        predictions_training = numpy.stack(predictions_training, axis=-1)
        predictions_validation = numpy.stack(predictions_validation, axis=-1)
        predictions_testing = numpy.stack(predictions_testing, axis=-1)

        self.log_confusion_matrix(predictions_testing, y_testing, "da")
        return {
            split: {
                "da": {
                    metric: getattr(self, metric)(
                        torch.tensor(predictions), trues, None, "da", movement_idxs
                    )
                    for metric in self.metrics["da"]
                }
            }
            for split, trues, predictions, movement_idxs in zip(
                ["training", "validation", "testing"],
                [y_training, y_validation, y_testing],
                [predictions_training, predictions_validation, predictions_testing],
                [movement_idxs_training, movement_idxs_validation, movement_idxs_testing],
            )
        }

    def evaluate_baseline_stim_rest(self, loaders):
        _, y_training = next(iter(loaders["training"]))
        _, y_validation = next(iter(loaders["validation"]))
        _, y_testing = next(iter(loaders["testing"]))

        # Extract stim from y
        y_training = y_training["stim"]
        y_validation = y_validation["stim"]
        y_testing = y_testing["stim"]

        predictions_training = numpy.zeros(y_training.shape)
        predictions_validation = numpy.zeros(y_validation.shape)
        predictions_testing = numpy.zeros(y_testing.shape)

        self.log_confusion_matrix(predictions_testing, y_testing, "stim")
        return {
            split: {
                "stim": {
                    metric: getattr(self, metric)(
                        torch.tensor(predictions), None, trues, "stim", None
                    )
                    for metric in self.metrics["stim"]
                }
            }
            for split, trues, predictions in zip(
                ["training", "validation", "testing"],
                [y_training, y_validation, y_testing],
                [predictions_training, predictions_validation, predictions_testing],
            )
        }

    def evaluate_baseline_stim_LDA(self, loaders):
        X_training, y_training = next(iter(loaders["training"]))
        X_validation, y_validation = next(iter(loaders["validation"]))
        X_testing, y_testing = next(iter(loaders["testing"]))

        # Reshape X
        X_training = X_training.reshape(len(X_training), -1)
        X_validation = X_validation.reshape(len(X_validation), -1)
        X_testing = X_testing.reshape(len(X_testing), -1)

        # Extract stim from y
        y_training = y_training["stim"].squeeze()
        y_validation = y_validation["stim"].squeeze()
        y_testing = y_testing["stim"].squeeze()

        lda = sklearn.discriminant_analysis.LinearDiscriminantAnalysis()
        lda.fit(X_training, y_training)
        predictions_training = lda.predict(X_training)
        predictions_validation = lda.predict(X_validation)
        predictions_testing = lda.predict(X_testing)

        self.log_confusion_matrix(predictions_testing, y_testing, "stim")
        return {
            split: {
                "stim": {
                    metric: getattr(self, metric)(
                        torch.tensor(predictions), None, trues, "stim", None
                    )
                    for metric in self.metrics["stim"]
                }
            }
            for split, trues, predictions in zip(
                ["training", "validation", "testing"],
                [y_training, y_validation, y_testing],
                [predictions_training, predictions_validation, predictions_testing],
            )
        }

    def accuracy_score(self, predictions, trues_da, trues_stim, output_type, movement_idxs):
        results = {}
        if output_type == "stim":
            macro_average = predictions == trues_stim
            results["macro_average"] = float(macro_average.sum() / len(macro_average))
            cm = sklearn.metrics.confusion_matrix(trues_stim, predictions)
            cm = cm.astype("float") / cm.sum(axis=1)[:, numpy.newaxis]
            accuracy_score_per_class = cm.diagonal()
            for i in range(len(accuracy_score_per_class)):
                results[f"class_{i}"] = float(accuracy_score_per_class[i])
        else:
            # Calculate general exact match ratio
            macro_average = numpy.count_nonzero(trues_da - predictions, axis=1) == 0
            results["macro_average"] = float(macro_average.sum() / len(macro_average))

            # Calculate exact match ratio per movement type
            for movement in range(len(self.gestures)):
                macro_average = (
                    numpy.count_nonzero(
                        trues_da[movement_idxs[movement]] - predictions[movement_idxs[movement]],
                        axis=1,
                    )
                    == 0
                )
                results[f"macro_average_movement_{movement}"] = (
                    float(macro_average.sum()) / len(macro_average)
                    if len(macro_average) > 0
                    else 0
                )

            # Calculate per output
            for output in range(predictions.shape[1]):
                cm = sklearn.metrics.confusion_matrix(trues_da[:, output], predictions[:, output])
                cm = cm.astype("float") / cm.sum(axis=1)[:, numpy.newaxis]
                accuracy_score_per_class = cm.diagonal()
                output_results = {
                    f"class_{i}": float(accuracy_score_per_class[i])
                    for i in range(len(accuracy_score_per_class))
                }
                output_results["macro_average"] = float(accuracy_score_per_class.mean())
                results[f"output_{output}"] = output_results
        return results

    def hamming_score(self, predictions, trues_da, trues_stim, output_type, movement_idxs):
        trues = trues_da if output_type == "da" else trues_stim
        results = {}

        # Calculate generally
        score = predictions == trues
        results["macro_average"] = float(score.sum() / (len(score) * predictions.shape[1]))

        # Calculate per movement
        for movement in range(len(self.gestures)):
            score = predictions[movement_idxs[movement]] == trues[movement_idxs[movement]]
            results[f"macro_average_movement_{movement}"] = float(
                score.sum() / (len(score) * predictions.shape[1])
            )

        return results

    def f1_score(self, predictions, trues_da, trues_stim, output_type, movement_idxs):
        trues = trues_da if output_type == "da" else trues_stim
        results = {}

        # Calculate generally
        scores = []
        for task in range(trues.shape[1]):
            scores.append(
                float(
                    sklearn.metrics.f1_score(
                        trues[:, task],
                        predictions[:, task],
                        average="macro",
                        zero_division=0,
                    )
                )
            )
        results["macro_average"] = sum(scores) / len(scores)

        if output_type == "da":
            # Calculate per output
            for output, score in enumerate(scores):
                results[f"output_{output}"] = score

            # Calculate per movement
            for movement in range(len(self.gestures)):
                scores = []
                for task in range(trues.shape[1]):
                    scores.append(
                        float(
                            sklearn.metrics.f1_score(
                                trues[movement_idxs[movement], task],
                                predictions[movement_idxs[movement], task],
                                average="macro",
                                zero_division=0,
                            )
                        )
                    )
                results[f"macro_average_movement_{movement}"] = sum(scores) / len(scores)
        return results

    def recall(self, predictions, trues_da, trues_stim, output_type, movement_idxs):
        trues = trues_da if output_type == "da" else trues_stim
        results = {}

        # Calculate generally
        scores = []
        for task in range(trues.shape[1]):
            scores.append(
                float(
                    sklearn.metrics.recall_score(
                        trues[:, task],
                        predictions[:, task],
                        average="macro",
                        zero_division=0,
                    )
                )
            )
        results["macro_average"] = sum(scores) / len(scores)

        if output_type == "da":
            # Calculate per output
            for output, score in enumerate(scores):
                results[f"output_{output}"] = score

            # Calculate per movement
            for movement in range(len(self.gestures)):
                scores = []
                for task in range(trues.shape[1]):
                    scores.append(
                        float(
                            sklearn.metrics.recall_score(
                                trues[movement_idxs[movement], task],
                                predictions[movement_idxs[movement], task],
                                average="macro",
                                zero_division=0,
                            )
                        )
                    )
                results[f"macro_average_movement_{movement}"] = sum(scores) / len(scores)
        return results

    def precision(self, predictions, trues_da, trues_stim, output_type, movement_idxs):
        trues = trues_da if output_type == "da" else trues_stim
        results = {}

        # Calculate generally
        scores = []
        for task in range(trues.shape[1]):
            scores.append(
                float(
                    sklearn.metrics.precision_score(
                        trues[:, task],
                        predictions[:, task],
                        average="macro",
                        zero_division=0,
                    )
                )
            )
        results["macro_average"] = sum(scores) / len(scores)

        if output_type == "da":
            # Calculate per output
            for output, score in enumerate(scores):
                results[f"output_{output}"] = score

            # Calculate per movement
            for movement in range(len(self.gestures)):
                scores = []
                for task in range(trues.shape[1]):
                    scores.append(
                        float(
                            sklearn.metrics.precision_score(
                                trues[movement_idxs[movement], task],
                                predictions[movement_idxs[movement], task],
                                average="macro",
                                zero_division=0,
                            )
                        )
                    )
                results[f"macro_average_movement_{movement}"] = sum(scores) / len(scores)
        return results

    def log_confusion_matrix(self, predictions, trues, output_type):
        if output_type == "stim":
            # Wandb plot
            wandb.log(
                {
                    "confusion_matrix": wandb.plot.confusion_matrix(
                        probs=None,
                        y_true=trues.squeeze().tolist(),
                        preds=predictions.squeeze().tolist(),
                        class_names=self.gestures,
                        title="Gesture",
                    )
                }
            )
        else:
            for output, (predictions_output, true_output) in enumerate(
                zip(predictions.swapaxes(0, 1), trues.swapaxes(0, 1))
            ):
                wandb.log(
                    {
                        f"confusion_matrix_{output}": wandb.plot.confusion_matrix(
                            probs=None,
                            y_true=true_output.tolist(),
                            preds=predictions_output.tolist(),
                            class_names=self.digit_actions,
                            title=self.digits[output],
                        )
                    }
                )
