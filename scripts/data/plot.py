import argparse
import math
import os
import sys

import matplotlib.pyplot as plt
import numpy
import seaborn

seaborn.set(font_scale=0.5)
seaborn.set_style("whitegrid", {"font.family": "serif"})


def plot_mu_and_norm(
    data,
    participant=1,
    exercise=1,
    run=1,
    sfreq=2000,
    bounds=[0, 30],
    start=0,
    end=30,
    save=None,
    **kwargs,
):
    # Load data
    norm = numpy.load(
        os.path.join(
            data,
            f"participant_{participant}",
            f"exercise_{exercise}",
            f"run_{run}",
            "envelope_norm.npy",
        )
    )[:, 0:10, 0]
    mulaw = numpy.load(
        os.path.join(
            data,
            f"participant_{participant}",
            f"exercise_{exercise}",
            f"run_{run}",
            "envelope_mulaw.npy",
        )
    )[:, 0:10, 0]

    # Set bounds
    start = max(bounds[0], 0.0)
    end = min(bounds[1], float(len(norm) / sfreq))
    length = end - start

    # Plot
    fig, (ax1, ax2) = plt.subplots(ncols=1, nrows=2, sharex=True)
    plt.xlabel("Time (s)")

    norm = norm[math.ceil(sfreq * start) : math.ceil(sfreq * end),]
    mulaw = mulaw[math.ceil(sfreq * start) : math.ceil(sfreq * end),]
    x = numpy.arange(0, length, step=length / (len(norm)))

    ax1.plot(x, norm)
    ax1.title.set_text("Min-Max Normalised")
    ax1.set_ylim(0, 1)
    ax2.plot(x, mulaw)
    ax2.title.set_text("μ-law Encoded")
    ax2.set_ylim(0, 1)
    fig.tight_layout(pad=1.0)

    if save != None:
        plt.savefig(save, bbox_inches="tight")
    plt.show()


def plot_raw_and_envelope(
    data,
    participant=1,
    exercise=1,
    run=1,
    sfreq=2000,
    bounds=[0, 30],
    start=0,
    end=30,
    save=None,
    **kwargs,
):
    # Load data
    env = numpy.load(
        os.path.join(
            data,
            f"participant_{participant}",
            f"exercise_{exercise}",
            f"run_{run}",
            "envelope.npy",
        )
    )[:, 0, :]
    emg = numpy.load(
        os.path.join(
            data,
            f"participant_{participant}",
            f"exercise_{exercise}",
            f"run_{run}",
            "raw.npy",
        )
    )[:, 0]

    # Set bounds
    start = max(bounds[0], 0.0)
    end = min(bounds[1], float(len(emg) / sfreq))
    length = end - start

    # Plot
    fig, axes = plt.subplots(ncols=1, nrows=env.shape[1] + 1, sharex=True)
    plt.xlabel("Time (s)")

    emg = emg[math.ceil(sfreq * start) : math.ceil(sfreq * end),]
    x = numpy.arange(0, length, step=length / (len(emg)))
    axes[0].plot(x, emg, color="r")
    axes[0].title.set_text("raw")
    axes[0].set_ylabel("mV")

    for i, (ax, cutoff) in enumerate(zip(axes[1:], [1, 3, 10])):
        env_i = env[math.ceil(sfreq * start) : math.ceil(sfreq * end), i]
        ax.plot(x, env_i, color="r")
        ax.set_ylabel("mV")
        ax.set_title(f"{cutoff}Hz")
    fig.tight_layout(pad=1.0)

    if save != None:
        plt.savefig(save, bbox_inches="tight")
    plt.show()


def plot_envelope(
    data,
    participant=1,
    sensor=0,
    order=0,
    exercise=1,
    run=1,
    sfreq=1000,
    bounds=[0, 30],
    start=0,
    end=30,
    save=None,
    no_axis=False,
    color=None,
    envelope_mulaw=True,
    **kwargs,
):
    # Load data
    print("gothere")
    X = numpy.load(
        os.path.join(
            data,
            f"participant_{participant}",
            f"exercise_{exercise}",
            f"run_{run}",
            f"envelope_{'mulaw' if envelope_mulaw else 'norm'}.npy",
        )
    )[:, :, order]
    if sensor is not None:
        X = X[:, sensor]

    # Plot
    start = max(bounds[0], 0.0)
    end = min(bounds[1], float(len(X) / sfreq))
    length = end - start
    y = X[math.ceil(sfreq * start) : math.ceil(sfreq * end),]
    x = numpy.arange(0, length, step=length / (len(y)))
    fig = plt.figure()
    ax = fig.add_subplot(111)
    if color is None:
        plt.plot(x, y)
    else:
        plt.plot(x, y, color=color)
    if no_axis:
        plt.gca().set_axis_off()
        ax.set_aspect("auto")
        plt.subplots_adjust(
            top=1, bottom=0, right=1, left=0, hspace=0, wspace=0
        )
        plt.margins(0, 0)
    else:
        plt.ylim(0, 1)
        plt.title("Envelope")
        plt.xlabel("Time (s)")
    if save != None:
        plt.savefig(save, bbox_inches="tight")
    plt.show()


def plot_raw(
    data,
    participant=1,
    sensor=0,
    order=0,
    exercise=1,
    run=1,
    sfreq=2000,
    bounds=[0, 30],
    save=None,
    no_axis=False,
    color=None,
    **kwargs,
):
    # Load data
    X = numpy.load(
        os.path.join(
            data,
            f"participant_{participant}",
            f"exercise_{exercise}",
            f"run_{run}",
            "raw.npy",
        )
    )
    if sensor is not None:
        X = X[:, sensor]

    # Plot
    start = max(bounds[0], 0)
    end = min(bounds[1], math.ceil(len(X) / sfreq))
    length = end - start
    y = X[int(sfreq * start) : int(sfreq * end),]
    x = numpy.arange(0, length, step=length / (len(y)))
    fig = plt.figure()
    ax = fig.add_subplot(111)
    if color is None:
        plt.plot(x, y)
    else:
        plt.plot(x, y, color=color)
    if no_axis:
        plt.gca().set_axis_off()
        ax.set_aspect("auto")
        plt.subplots_adjust(
            top=1, bottom=0, right=1, left=0, hspace=0, wspace=0
        )
        plt.margins(0, 0)
    else:
        plt.title("EMG")
        plt.xlabel("Time (s)")
    if save != None:
        plt.savefig(save, bbox_inches="tight")
    plt.show()


def plot_feature(
    data,
    feature,
    participant=1,
    sensor=None,
    exercise=1,
    run=1,
    sfreq=2000,
    window_stride=50,
    bounds=[0, 30],
    save=None,
    **kwargs,
):
    # Load data
    X = numpy.load(
        os.path.join(
            data,
            f"participant_{participant}",
            f"exercise_{exercise}",
            f"run_{run}",
            f"{feature}.npy",
        )
    )
    if sensor is not None:
        X = X[:, sensor]
    windowed_sfreq = math.floor(sfreq / window_stride)

    # Plot
    start = max(bounds[0], 0)
    end = min(bounds[1], math.ceil(len(X) / windowed_sfreq))
    length = end - start
    y = X[int(start * windowed_sfreq) : int((end * windowed_sfreq)),]
    x = numpy.arange(0, length, step=length / (len(y)))
    plt.plot(x, y)
    plt.xlabel("Time (s)")
    plt.ylabel(feature)
    if save != None:
        plt.savefig(save, bbox_inches="tight")
    plt.show()


def plot_da(
    data, participant=1, exercise=1, run=1, bounds=[0, 30], save=None, **kwargs
):
    # Load data
    angles = numpy.load(
        os.path.join(
            data,
            f"participant_{participant}",
            f"exercise_{exercise}",
            f"run_{run}",
            f"glove.npy",
        )
    )
    y = numpy.load(
        os.path.join(
            data,
            f"participant_{participant}",
            f"exercise_{exercise}",
            f"run_{run}",
            f"digit_action_labels.npy",
        )
    )

    # Plot result
    inc = 5e-2
    figsize = (6.8, 3.5)
    c1, c2 = "grey", "k"
    ls1, ls2 = "--", "-."
    lw1, lw2 = 2.0, 2.0
    ss1, ss2 = 0.1, 0.1

    dofs = [
        "Thumb rotation",
        "Thumb flexion",
        "Index flexion",
        "Middle flexion",
        "Ring flexion",
        "Little flexion",
    ]

    t_vec = numpy.arange(0, angles.shape[0] * inc, step=inc)

    start_ind = int(bounds[0] / inc)
    end_ind = int(bounds[1] / inc)
    fig = plt.figure(figsize=figsize)
    for dof_index in range(6):
        ax1 = fig.add_subplot(3, 2, dof_index + 1)
        ax1.scatter(
            t_vec[start_ind:end_ind],
            angles[start_ind:end_ind, dof_index],
            color=c1,
            linestyle=ls1,
            linewidth=lw1,
            s=ss1,
        )
        ax1.set_ylabel("Position", color=c1)
        ax1.tick_params(axis="y", labelcolor=c1)
        ax1.set_ylim([-0.05, 1.05])
        ax1.set_yticks([0, 0.5, 1.0])
        ax1.set_title(dofs[dof_index])

        ax2 = ax1.twinx()
        ax2.scatter(
            t_vec[start_ind:end_ind],
            y[start_ind:end_ind, dof_index],
            color=c2,
            linestyle=ls2,
            linewidth=lw2,
            s=ss2,
        )
        ax2.set_ylabel("Action", color=c2)
        ax2.tick_params(axis="y", labelcolor=c2)
        ax2.set_yticks([0, 1, 2])
        ax2.set_ylim([-0.1, 2.1])
        ax2.set_yticklabels(["open", "stall", "close"])

        # Reset timing in x-axis
        xtcks = ax1.get_xticks()[1:-1]
        for ax in [ax1, ax2]:
            ax.set_xticks(xtcks)
            ax.set_xticklabels((xtcks - bounds[0]).astype(numpy.intc))

        if dof_index in [4, 5]:
            ax1.set_xlabel("Time [s]")
        else:
            ax1.set_xticklabels(ax1.get_xticklabels(), visible=False)

    fig.tight_layout()
    plt.subplots_adjust(hspace=0.4)
    if save != None:
        plt.savefig(save, bbox_inches="tight")
    plt.show()


if __name__ == "__main__":
    # Add project root to path
    sys.path.append(os.getcwd())

    # Read calling arguments
    parser = argparse.ArgumentParser(
        description="Transformer-Based Myoelectric Digit Action Decoding: data plotting"
    )
    parser.add_argument("data", type=str)
    parser.add_argument("--save", type=str, default=None)
    parser.add_argument("--no_axis", action="store_true", default=False)
    parser.add_argument("--color", type=str, default=None)
    parser.add_argument("--raw", action="store_true", default=False)
    parser.add_argument("--envelope", action="store_true", default=False)
    parser.add_argument(
        "--raw_and_envelope", action="store_true", default=False
    )
    parser.add_argument("--mu_and_norm", action="store_true", default=False)
    parser.add_argument("--sensor", type=int, default=None)
    parser.add_argument("--order", type=int, default=0)
    parser.add_argument("--da", action="store_true", default=False)
    parser.add_argument(
        "-f",
        "--feature",
        default=False,
        type=str,
    )

    parser.add_argument("-p", "--participant", type=int, default=1)
    parser.add_argument("-e", "--exercise", type=int, default=1)
    parser.add_argument("-r", "--run", type=int, default=1)
    parser.add_argument(
        "-b", "--bounds", nargs="+", type=float, default=[0, 30]
    )
    args = parser.parse_args()

    # Plot
    if args.feature:
        plot_feature(**vars(args))
    if args.raw:
        plot_raw(**vars(args))
    if args.envelope:
        plot_envelope(**vars(args))
    if args.da:
        plot_da(**vars(args))
    if args.raw_and_envelope:
        plot_raw_and_envelope(**vars(args))
    if args.mu_and_norm:
        plot_mu_and_norm(**vars(args))
