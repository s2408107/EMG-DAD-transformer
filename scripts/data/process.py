import argparse
import functools
import multiprocessing
import os
import shutil
import sys

import numpy
import scipy.io
import scipy.signal
import sklearn.preprocessing
import torch
import torchaudio

# Add project root to path
sys.path.append(os.getcwd())

import src.processing as processing


def process_participant(
    dataset_raw, dataset_processed, dataset_config, participant
):
    print(f"- participant {participant}:")

    # Make participant folder
    participant_folder = os.path.join(
        dataset_processed, f"participant_{participant}"
    )
    os.mkdir(participant_folder)

    for exercise in range(1, 1 + dataset_config.num_exercises):
        print(f" # exercise {exercise}:")

        # Make exercise folder
        exercise_folder = os.path.join(
            participant_folder, f"exercise_{exercise}"
        )
        os.mkdir(exercise_folder)

        envelope_min, envelope_ptp = 0, 0
        glove_min, glove_ptp = 0, 0
        features_mean, features_std = {}, {}
        for run in range(1, 1 + dataset_config.num_runs):
            print(f"  * run {run}:")

            # Make exercise folder
            run_folder = os.path.join(exercise_folder, f"run_{run}")
            os.mkdir(run_folder)

            # Read .mat file
            data = scipy.io.loadmat(
                os.path.join(dataset_raw, f"S{participant}_E{exercise}_A{run}")
            )
            stimulus = data["restimulus"]
            repetition = data["rerepetition"]
            emg = data["emg"]
            glove = data["glove"] if "glove" in data else None

            # Strip ending zeros
            before = len(emg)
            nz = numpy.nonzero(emg)
            emg = emg[
                nz[0].min() : nz[0].max() + 1, nz[1].min() : nz[1].max() + 1
            ]
            stimulus = stimulus[: len(emg)]
            repetition = repetition[: len(emg)]
            glove = glove[: len(emg)]

            # -------------------------------------
            # REPETITION
            # -------------------------------------

            # Remove 0 reptition
            nonzero_repetition = numpy.where(repetition != 0)[0]
            repetition = repetition[nonzero_repetition]
            emg = emg[nonzero_repetition]
            stimulus = stimulus[nonzero_repetition]
            glove = glove[nonzero_repetition]

            # Cut 1s -> edge artifacts
            repetition = repetition[: -int(dataset_config.emg_sfreq)]
            stimulus = stimulus[: -int(dataset_config.emg_sfreq)]
            glove = glove[: -int(dataset_config.emg_sfreq)]

            # Window
            repetition = processing.window(
                repetition,
                dataset_config.window_size,
                dataset_config.window_stride,
                dataset_config.emg_sfreq,
            ).squeeze()

            # Print stats
            num_repetitions = len(numpy.unique(repetition))
            print(f"   + {len(repetition)} repetition windows")
            print(f"   + {num_repetitions} repetitions")

            # Majority vote
            repetition = numpy.stack(
                [
                    numpy.bincount(window, minlength=num_repetitions).argmax()
                    for window in repetition
                ]
            )

            # Save
            numpy.save(
                os.path.join(run_folder, "repetition"),
                repetition,
            )
            del repetition

            # -------------------------------------
            # STIMULUS
            # -------------------------------------

            # Create class ids
            uniques = numpy.unique(stimulus)
            class_ids = {
                unique: id for unique, id in zip(uniques, range(len(uniques)))
            }
            stimulus = numpy.vectorize(class_ids.get)(stimulus)

            # Window
            stimulus = processing.window(
                stimulus,
                dataset_config.window_size,
                dataset_config.window_stride,
                dataset_config.emg_sfreq,
            ).squeeze()

            # Majority vote
            stimulus = numpy.stack(
                [
                    numpy.bincount(window, minlength=len(class_ids)).argmax()
                    for window in stimulus
                ]
            )

            # Print class freqs
            uniques, counts = numpy.unique(stimulus, return_counts=True)
            print(f"   + {len(stimulus)} stimulus windows")
            print("   + stimulus class freqs:")
            for unique, count in zip(uniques, counts):
                print(f"      > {unique} : {count/len(stimulus)}")

            # Save
            numpy.save(
                os.path.join(run_folder, "stimulus"),
                stimulus,
            )
            del stimulus

            # -------------------------------------
            # EMG
            # -------------------------------------

            # -----------
            # Raw
            # -----------

            # Filter
            filtered = processing.butterworth_filter(
                emg,
                dataset_config.emg_sfreq,
                [
                    dataset_config.features_lowfreq,
                    dataset_config.features_highfreq,
                ],
                dataset_config.features_filter_order,
                "band",
                "filtfilt",
            )

            numpy.save(
                os.path.join(run_folder, "raw"),
                filtered,
            )

            # -----------
            # Envelope
            # -----------

            if args.envelope:
                # Filter
                envelope = numpy.stack(
                    [
                        processing.butterworth_filter(
                            abs(filtered),
                            dataset_config.emg_sfreq,
                            dataset_config.envelope_highfreq,
                            # highfreq,
                            order,
                            "low",
                            "filtfilt",
                        )
                        for order in dataset_config.envelope_filter_orders
                        # for highfreq in [1.0, 3.0, 10.0]
                    ],
                    axis=-1,
                )

                # Downsample if required
                if dataset_config.emg_sfreq != dataset_config.emg_new_sfreq:
                    envelope = scipy.signal.resample(
                        envelope,
                        int(
                            len(envelope)
                            * (
                                dataset_config.emg_new_sfreq
                                / dataset_config.emg_sfreq
                            )
                        ),
                        axis=0,
                    )

                # Cut off 1 second to avoid artifacts
                envelope = envelope[: -int(dataset_config.emg_new_sfreq)]

                # Min-max normalisation
                if run == 1:
                    envelope_min = envelope.min()
                    envelope_ptp = envelope.ptp()
                envelope = (envelope - envelope_min) / envelope_ptp
                numpy.save(
                    os.path.join(run_folder, "envelope_norm"),
                    envelope,
                )

                # Mulaw encoding
                envelope = numpy.sign(envelope) * (
                    numpy.log1p(255 * abs(envelope)) / numpy.log1p(255)
                )
                numpy.save(
                    os.path.join(run_folder, "envelope_mulaw"),
                    envelope,
                )

                del envelope

            # -----------
            # Features
            # -----------

            if args.features:
                # Window
                windows = processing.window(
                    filtered,
                    dataset_config.window_size,
                    dataset_config.window_stride,
                    dataset_config.emg_sfreq,
                )

                # Extract features
                for feature in [
                    "stfs",
                    "logvar",
                    "waveform_length",
                    "hjorth_activity",
                    "hjorth_mobility",
                    "hjorth_complexity",
                    "wilson_amplitude",
                    "kurtosis",
                    "skewness",
                    "ar",
                ]:
                    features = getattr(processing, feature)(windows, axis=2)
                    if run == 1:
                        features_mean[feature] = features.reshape(
                            features.shape[-1], -1
                        ).mean(axis=1)
                        features_std[feature] = features.reshape(
                            features.shape[-1], -1
                        ).std(axis=1)
                    features = (
                        features - features_mean[feature]
                    ) / features_std[feature]
                    numpy.save(os.path.join(run_folder, feature), features)

            # -------------------------------------
            # KINETIC
            # -------------------------------------

            if glove is not None:
                # Map glove to original matrix
                if dataset_config.glove_map is not None:
                    glove = glove[
                        :,
                        [
                            idx - 1
                            for idx in list(dataset_config.glove_map.values())
                        ],
                    ]

                # Linear transform
                A = numpy.array(
                    [
                        [0.639, 0, 0, 0, 0, 0],
                        [0.383, 0, 0, 0, 0, 0],
                        [0, 1, 0, 0, 0, 0],
                        [-0.639, 0, 0, 0, 0, 0],
                        [0, 0, 0.4, 0, 0, 0],
                        [0, 0, 0.6, 0, 0, 0],
                        [0, 0, 0, 0.4, 0, 0],
                        [0, 0, 0, 0.6, 0, 0],
                        [0, 0, 0, 0, 0, 0],
                        [0, 0, 0, 0, 0.3333, 0],
                        [0, 0, 0, 0, 0.6666, 0],
                        [0, 0, 0, 0, 0, 0],
                        [0, 0, 0, 0, 0, 0.3333],
                        [0, 0, 0, 0, 0, 0.6666],
                        [0, 0, 0, 0, 0, 0],
                        [-0.19, 0, 0, 0, 0, 0],
                        [0, 0, 0, 0, 0, 0],
                        [0, 0, 0, 0, 0, 0],
                    ]
                )
                glove = glove @ A

                # Window
                glove = processing.window(
                    glove,
                    dataset_config.window_size,
                    dataset_config.window_stride,
                    dataset_config.emg_sfreq,
                ).mean(
                    axis=2
                )  # average
                print(f"   + {len(glove)} glove windows")

                # Filter
                glove = processing.butterworth_filter(
                    glove,
                    dataset_config.glove_sfreq,
                    dataset_config.glove_highfreq,
                    dataset_config.glove_filter_order,
                    "low",
                    "filtfilt",
                )

                # MinMax scale
                if run == 1:
                    glove_min = glove.min(axis=0)
                    glove_ptp = glove.ptp(axis=0)
                glove = (glove - glove_min) / glove_ptp

                # Calculate digit action labels
                digit_action_labels = processing.compute_digit_action_labels(
                    glove,
                    dataset_config.num_digits,
                    dataset_config.tolerance,
                    dataset_config.tolerance_limit,
                )

                # Print class freqs
                _, counts = numpy.unique(
                    digit_action_labels, return_counts=True
                )
                print(
                    "   + digit action class freqs:\n"
                    f"    > open : {numpy.around(counts[0]/(len(glove)*dataset_config.num_digits), 4)}\n"
                    f"    > stall: {numpy.around(counts[1]/(len(glove)*dataset_config.num_digits), 4)}\n"
                    f"    > close: {numpy.around(counts[2]/(len(glove)*dataset_config.num_digits), 4)}"
                )

                # Save
                numpy.save(
                    os.path.join(run_folder, "digit_action_labels"),
                    digit_action_labels,
                )
                numpy.save(
                    os.path.join(run_folder, "glove"),
                    glove,
                )
                del digit_action_labels, glove


def process(dataset_config, args):
    # Folder where raw data is
    dataset_raw = os.path.join(dataset_config.dataset_dir, "raw")

    # Folder where processed data will be
    dataset_processed = os.path.join(
        dataset_config.dataset_dir, dataset_config.dataset_processed
    )

    # Process in parallel
    pool = multiprocessing.Pool(args.parallel)
    pool.map(
        functools.partial(
            process_participant, dataset_raw, dataset_processed, dataset_config
        ),
        range(1, 1 + dataset_config.num_participants),
    )


if __name__ == "__main__":
    # Add project root to path
    sys.path.append(os.getcwd())

    # Read calling arguments
    parser = argparse.ArgumentParser(
        description="Transformer-Based Myoelectric Digit Action Decoding: data processing"
    )
    parser.add_argument("config", type=str)
    parser.add_argument("--storage", type=str, default=os.getcwd())
    parser.add_argument("--parallel", type=int, default=4)
    parser.add_argument("--delete_old", action="store_true", default=False)
    parser.add_argument("--envelope", action="store_true", default=False)
    parser.add_argument("--features", action="store_true", default=False)
    args = parser.parse_args()

    # Read configuration
    from src import utils
    from src.config import Config

    config = Config(**utils.load_yaml(args.config))

    # Add storage to dataset_dir
    config.dataset_config.dataset_dir = os.path.join(
        args.storage, config.dataset_config.dataset_dir
    )

    # Verify if downloaded
    if not os.path.exists(config.dataset_config.dataset_dir):
        print("Data folder does not exist!")
        quit()

    # Verify if output folder exists
    if (
        os.path.exists(
            os.path.join(
                config.dataset_config.dataset_dir,
                config.dataset_config.dataset_processed,
            )
        )
        and args.delete_old
    ):
        shutil.rmtree(
            os.path.join(
                config.dataset_config.dataset_dir,
                config.dataset_config.dataset_processed,
            )
        )
    elif os.path.exists(
        os.path.join(
            config.dataset_config.dataset_dir,
            config.dataset_config.dataset_processed,
        )
    ):
        print("Output folder exists!")
        quit()
    os.mkdir(
        os.path.join(
            config.dataset_config.dataset_dir,
            config.dataset_config.dataset_processed,
        )
    )

    # Set seed
    utils.setup_random_seed(config.training_config.random_seed)

    # Process the data
    process(config.dataset_config, args)
