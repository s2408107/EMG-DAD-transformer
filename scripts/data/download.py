import argparse
import os
import sys
import zipfile

import wget

if __name__ == "__main__":
    # Add project root to path
    sys.path.append(os.getcwd())

    # Read calling arguments
    parser = argparse.ArgumentParser(
        description="Transformer-Based Myoelectric Digit Action Decoding: data downloading"
    )
    parser.add_argument("--config", type=str, default="configs/tryout.yaml")
    parser.add_argument("--storage", type=str, default=os.getcwd())
    args = parser.parse_args()

    # Read configuration
    from src import utils
    from src.config import Config

    config = Config(**utils.load_yaml(args.config))

    # Download the data
    for participant in range(1, 1 + config.dataset_config.num_participants):
        for exercise in range(1, 1 + config.dataset_config.num_exercises):
            for run in range(1, 1 + config.dataset_config.num_runs):
                # Verify if downloaded
                file_path = os.path.join(
                    args.storage,
                    config.dataset_config.dataset_dir,
                    f"raw/S{participant}_E{exercise}_A{run}.mat",
                )
                if not os.path.exists(file_path):
                    print(
                        f"\nDownloading run {run} of exercise {exercise} of participant {participant}:"
                    )

                    # Download data
                    wget.download(
                        f"http://ninapro.hevs.ch/system/files/DB8/S{participant}_E{exercise}_A{run}.mat",
                        out=file_path,
                    )
