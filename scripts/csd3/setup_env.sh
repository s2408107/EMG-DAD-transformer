#!/bin/bash


conda install pytorch torchvision torchaudio pytorch-cuda=11.7 -c pytorch -c nvidia
conda install -c conda-forge ray-tune bayesian-optimization wandb scipy scikit-learn 
pip install randomname git+https://github.com/agamemnonc/sklearn-ext.git