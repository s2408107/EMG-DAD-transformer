#!/bin/bash

## INPUT ARGS:
# $1: config file path
# $2: output dir name

# Bash script
sbatch <<EOT
#!/bin/bash
#SBATCH --job-name=EDT-hpo
#SBATCH --output=$2_%A.out
#SBATCH -N 1
#SBATCH -n 1
#SBATCH --gres=gpu:1
#SBATCH --cpus-per-gpu=32
#SBATCH --mem=256000
#SBATCH --partition=ampere
#SBATCH --account=BMAI-CDT-SL2-GPU
#SBATCH -t 24:00:00

. /etc/profile.d/modules.sh                # Enables the module command
module purge                               # Removes all modules still loaded
module load rhel8/default-amp              # Loads the basic environment

# Load required modules
module load cuda/11.1 cudnn/8.0_cuda-11.1

# Activate env
source /rds/user/hpcwulf1/hpc-work/emg_dad_transformer/env/bin/activate

# Run
python scripts/training/hpo.py $1 --name=$2 --storage=/rds/user/hpcwulf1/hpc-work/emg_dad_transformer --cpus=32

echo "============"
echo "SLURM JOB DONE"
EOT