import argparse
import collections
import itertools
import json
import math

import matplotlib.pyplot as plt
import numpy
import pandas
import seaborn
import statannot

import wandb


def test(args):
    seaborn.set_style("whitegrid")
    seaborn.color_palette("colorblind")
    font = {"family": "serif", "size": 14}
    plt.rc("font", **font)

    # Set groups
    groups = {
        "da_stall": "All stall",
        "da_LDA": "LDA",
        "da_features": "FTR",
        "da_full_1f_pretrained": "FULL",
        "da_fnet_1f": "FNET",
        "da_tnet_1f": "TNET",
        "da_full_1f": "FULL*",
        # "da_full_1f_pretrained_mm": "FULL\nMM",
        # "da_full_3f_pretrained": "FULL\n<3Hz",
        # "da_full_1f_pretrained_weighted": "FULL\nWCE",
        # "da_full_1f_pretrained_heads": "FULL\n6 heads",
        # "da_LDA": "LDA",
        "da_LDA_mix": "LDA\nMIX",
        "da_LDA_mixl": "LDA\nMIXL",
        "da_full_1f_pretrained": "FULL",
        "da_full_1f_pretrained_mix": "FULL\nMIX",
        "da_full_1f_pretrained_mixl": "FULL\nMIXL",
    }
    metrics = {
        f"testing.da.{metric}": metric.replace("_", " ")
        for metric in [
            "f1_score.macro_average",
            # "accuracy_score.macro_average",
            # "hamming_score.macro_average",
            # "recall.macro_average",
            # "precision.macro_average",
        ]
    }
    # groups = {
    #    "stim_rest": "All rest",
    #    "stim_LDA": "LDA",
    #    "stim_full_1f": "FULL",
    #    "stim_fnet_1f": "FNET",
    #    "stim_tnet_1f": "TNET",
    # }
    # metrics = {
    #    f"testing.stim.{metric}": metric.replace("_", " ")
    #    for metric in ["accuracy_score.macro_average"]
    # }

    # Get data
    api = wandb.Api()
    runs = api.runs(
        "wulfdewolf/EMG-DAD-transformer",
        filters={"group": {"$in": list(groups.keys())}},
    )
    results = collections.defaultdict(lambda: {})
    for run in runs:
        for metric in metrics.keys():
            results[run.name][
                (metrics[metric], groups[run.group])
            ] = run.history(keys=[metric]).iloc[0][metric]
    data = pandas.DataFrame.from_dict(results, orient="index")

    # Make medians table
    print(data)
    medians_table = data.median(axis=0)
    print(medians_table)
    mins_table = data.min(axis=0)
    maxs_table = data.max(axis=0)
    new_medians_table = {}
    for group_name in groups.values():
        medians = [
            round(medians_table[metric_name][group_name], 2)
            for metric_name in metrics.values()
        ]
        mins = [
            round(mins_table[metric_name][group_name], 2)
            for metric_name in metrics.values()
        ]
        maxs = [
            round(maxs_table[metric_name][group_name], 2)
            for metric_name in metrics.values()
        ]
        new_medians_table[group_name] = [
            f"{median:.2f} ({min:.2f}, {max:.2f})"
            for median, min, max in zip(medians, mins, maxs)
        ]
    medians_table = pandas.DataFrame.from_dict(
        new_medians_table, orient="index", columns=list(metrics.values())
    )
    print(medians_table.to_latex())

    # Make figure
    fig, axes = plt.subplots(ncols=1, nrows=len(metrics), sharex=True)
    for metric, ax in zip(
        metrics.values(), [axes] if len(metrics) == 1 else axes
    ):
        metric_data = data[metric][groups.values()]
        seaborn.boxplot(
            metric_data, ax=ax, showfliers=False, order=list(groups.values())
        )
        seaborn.stripplot(
            metric_data,
            size=3,
            ax=ax,
            palette="dark:black",
            order=list(groups.values()),
        )
        ax.set(
            xlabel="",
            # ylabel="Classification Accuracy",
            ylabel="F1-score (macro average)"
            if "f1" in metric
            else "Exact match ratio",
            ylim=(0, 1),
        )
        # Add actual values
        medians = [
            round(numpy.median(data[metric][group]), 3)
            for group in groups.values()
        ]
        best_idx = numpy.argmax(medians)
        max = [
            round(numpy.max(data[metric][group]), 3)
            for group in groups.values()
        ]
        for xtick in ax.get_xticks():
            ax.text(
                xtick,
                max[xtick] + 0.05,
                f"{medians[xtick]}",
                horizontalalignment="center",
                color="black",
            )
        # Add test accolades
        plot = statannot.add_stat_annotation(
            ax,
            data=metric_data,
            box_pairs=[
                (list(groups.values())[best_idx], group)
                for group in list(groups.values())[:best_idx]
                + list(groups.values())[best_idx + 1 :]
            ],
            test="t-test_paired",
            text_format="star",
            pvalue_thresholds=[[0.05, "*"], [1, "ns"]],
            loc="outside",
            verbose=2,
        )

    if args.save is not None:
        ax.get_figure().savefig(args.save, bbox_inches="tight")
    plt.tight_layout()
    plt.show()


if __name__ == "__main__":
    # Read calling arguments
    parser = argparse.ArgumentParser(
        description="Transformer-Based Myoelectric Digit Action Decoding: testing"
    )
    parser.add_argument("--save", type=str, default=None)
    args = parser.parse_args()

    test(args)
