import argparse
import os
import sys

import matplotlib.pyplot as plt
import numpy
import seaborn
import sklearn
import sklearn.decomposition
import torch
import yaml

# Add project root to path
sys.path.append(os.getcwd())

from src import utils
from src.config import Config
from src.evaluator import Evaluator
from src.trainer import Trainer


def script(args, config):
    # Set seed
    utils.setup_random_seed(config.training_config.random_seed)

    # Set device
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

    # Create evaluator
    evaluator = Evaluator(
        device,
        config.dataset_config.gestures,
        config.dataset_config.digits,
        config.dataset_config.digit_actions,
    )

    # Create trainer
    trainer = Trainer(
        evaluator, device, config, storage=os.getcwd(), log_to_wandb=False, optimising=False
    )

    # Train
    results, model, loaders = trainer.train_participant(args.participant, "")

    # Print evaluation
    print(yaml.dump(results, allow_unicode=True, default_flow_style=False))

    # Get transformer output tokens
    tokens = []
    ys = []
    with torch.no_grad():
        for batch_X, batch_y in loaders["training"]:
            tokens.append(
                model(batch_X.to(device, dtype=torch.float32), heads=False)[
                    (config.model_config.nets[0], config.model_config.outputs[0])
                ]
                .cpu()
                .detach()
            )
            ys.append(batch_y)
    tokens = torch.concatenate(tokens, axis=0)[:, 0]
    ys = torch.concatenate([y["da"] for y in ys], axis=0)[:, 0]
    print(ys.shape)

    # PCA
    tokens_PCA = sklearn.decomposition.PCA(n_components=3).fit_transform(
        sklearn.preprocessing.StandardScaler().fit_transform(tokens)
    )

    # Plot
    Xax = tokens_PCA[:, 0]
    Yax = tokens_PCA[:, 1]
    Zax = tokens_PCA[:, 2]
    fig = plt.figure(figsize=(7, 5))
    ax = fig.add_subplot(111, projection="3d")
    colors = {
        i: color
        for color, (i, _) in zip(
            seaborn.color_palette("colorblind"), enumerate(config.dataset_config.digit_actions)
        )
    }
    labels = {
        i: digit_action for i, digit_action in enumerate(config.dataset_config.digit_actions)
    }

    fig.patch.set_facecolor("white")
    for l in numpy.unique(ys):
        ix = numpy.where(ys == l)
        ax.scatter(
            Xax[ix],
            Yax[ix],
            Zax[ix],
            c=colors[l],
            s=40,
            label=labels[l],
            # marker=marker[l],
            alpha=0.3,
        )
    plt.legend()
    plt.show()


if __name__ == "__main__":
    # Read calling arguments
    parser = argparse.ArgumentParser(
        description="Transformer-Based Myoelectric Digit Action Decoding: common training"
    )
    parser.add_argument("config", type=str)
    parser.add_argument("participant", type=int)
    args = parser.parse_args()

    # Read configuration
    config = Config(**utils.load_yaml(args.config))

    # Call script
    script(args, config)
