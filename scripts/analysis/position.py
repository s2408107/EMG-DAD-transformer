import argparse
import os
import re

import matplotlib.pyplot as plt
import numpy
import torch


def plot_positional_encoding(group, save):
    positional_encodings = {}
    for participant in range(1, 13):
        model = torch.load(
            os.path.join(
                os.getcwd(),
                "models",
                group,
                f"participant_{participant}.pt",
            )
        )
        positional_encodings[participant] = model["positional_embedding"]

    # Calculate similarities
    similarity_function = torch.nn.CosineSimilarity(dim=0, eps=1e-6)
    similarities = {}
    for name, positional_encoding in positional_encodings.items():
        similarity = numpy.zeros(
            (len(positional_encoding), len(positional_encoding))
        )
        for i in range(len(positional_encoding)):
            for j in range(len(positional_encoding)):
                similarity[i, j] = similarity_function(
                    positional_encoding[i], positional_encoding[j]
                )
        similarities[name] = similarity

    # Plot
    font = {"family": "serif", "size": 14}
    plt.rc("font", **font)
    fig, axes = plt.subplots(
        figsize=(12, 6),
        ncols=4,
        nrows=3,
    )
    plt.subplots_adjust(hspace=0.4)
    for (participant, similarity), ax in zip(
        list(similarities.items()), axes.reshape(-1)
    ):
        im = ax.imshow(
            similarity,
            cmap="hot",
            vmin=-1,
            vmax=1,
            extent=[0, 16, 16, 0],
            origin="upper",
        )
        if participant == 9:
            ax.xaxis.set_ticks(
                [i - 0.5 for i in range(1, len(similarity) + 1, 15)],
                range(1, len(similarity) + 1, 15),
            )
            ax.yaxis.set_ticks(
                [i - 0.5 for i in range(1, len(similarity) + 1, 15)],
                range(1, len(similarity) + 1, 15),
            )
        else:
            ax.xaxis.set_ticks([], [])
            ax.yaxis.set_ticks([], [])
        ax.title.set_text(participant)
    fig.tight_layout()
    fig.colorbar(im, ax=axes, orientation="vertical")
    if save != None:
        plt.savefig(args.save, bbox_inches="tight")
    plt.show()


if __name__ == "__main__":
    # Read calling arguments
    parser = argparse.ArgumentParser(
        description="Transformer-Based Myoelectric Digit Action Decoding: positional encodings"
    )
    parser.add_argument("group", type=str)
    parser.add_argument("--save", type=str, default=None)
    args = parser.parse_args()

    # Call script
    plot_positional_encoding(**args.__dict__)
