import argparse
import os
import sys

import randomname
import torch
import wandb
import yaml

# Add project root to path
sys.path.append(os.getcwd())

from src import utils
from src.config import Config
from src.dataset import EMGDataset
from src.evaluator import Evaluator


def evaluate_metrics(args, config):
    random_id = randomname.get_name()

    # Set seed
    utils.setup_random_seed(config.training_config.random_seed)

    # Set device
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

    # Create evaluator
    evaluator = Evaluator(
        device,
        config.dataset_config.gestures,
        config.dataset_config.digits,
        config.dataset_config.digit_actions,
    )

    # Loop over participants
    participants_results = []
    for participant in range(1, 1 + config.dataset_config.num_participants):
        # Initialise W&B
        run = wandb.init(
            project="EMG-DAD-transformer",
            group=f"{args.name}_{random_id}",
            name=f"partcipant {participant}",
            config=config.dict(),
            reinit=True,
            mode="online" if args.log_to_wandb else "disabled",
        )

        # Set hyperparams to specific or common hyperparams
        hyperparams = config.hyperparams[0 if len(config.hyperparams) == 1 else participant - 1]

        # Create dataset
        dataset = EMGDataset(
            os.getcwd(),
            participant,
            features=config.model_config.net == "FTR",
            **config.dataset_config.dict(),
        )
        loaders = dataset.get_dataloaders(hyperparams.batch_size)

        # Read model
        dataset_model_args = dict(
            participant=participant,
            num_digits=dataset.num_digits,
            num_digit_actions=dataset.num_digit_actions,
            num_gestures=dataset.num_gestures,
            num_sensors=dataset.num_sensors,
            window_size_in_samples=dataset.window_size_in_samples,
            envelope_patch_window=dataset.envelope_patch_window,
            num_features=dataset.num_features,
        )
        model = utils.model_class(config.model_config.net)(
            **dataset_model_args, **hyperparams.dict(), **config.model_config.dict()
        )
        model.load_state_dict(
            torch.load(
                os.path.join(os.getcwd(), "models", args.name, f"participant_{participant}.pt")
            )
        )
        model.to(device=device)

        # Calculate metrics
        model.eval()
        results = evaluator.evaluate(model, loaders["testing"], log_cm=True)

        # Log
        wandb.log(dict(testing=results))
        participants_results.append(results)

    # End W&B run
    run.finish()

    # Calculate mean/median/... over participants
    results = {
        statistic: utils.calculate_statistic(statistic, participants_results)
        for statistic in ["mean", "median"]
    }

    # Print final results
    print("-" * 50 + f"\nRESULTS:\n")
    print(yaml.dump(results, allow_unicode=True, default_flow_style=False))


if __name__ == "__main__":
    # Read calling arguments
    parser = argparse.ArgumentParser(
        description="Transformer-Based Myoelectric Digit Action Decoding: evaluating metrics"
    )
    parser.add_argument("config", type=str)
    parser.add_argument("name", type=str, default=randomname.get_name())
    parser.add_argument("--log_to_wandb", action="store_true", default=False)
    args = parser.parse_args()

    # Read configuration
    config = Config(**utils.load_yaml(args.config))

    # Call script
    evaluate_metrics(args, config)
