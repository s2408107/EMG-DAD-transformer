import argparse
import json
import os

import matplotlib.pyplot as plt
import numpy
import pandas
import seaborn

import wandb


def get_cm(run, is_da):
    cms = []
    columns = None
    for i, artifact in enumerate(run.logged_artifacts()):
        if "confusion" not in artifact._artifact_name:
            continue

        # Save confusion matrix artifact
        artifact.download("/tmp/artifact")

        # Read as json
        file_name = (
            f"/tmp/artifact/confusion_matrix_{i}_table.table.json"
            if is_da
            else "/tmp/artifact/confusion_matrix_table.table.json"
        )
        with open(file_name, "r") as f:
            cm = json.load(f)
        cm_table = (
            pandas.DataFrame(cm["data"], columns=cm["columns"])
            .groupby(["Actual", "Predicted"])
            .mean()
            .reset_index()
        )
        if columns is None:
            columns = cm_table["Actual"].unique().tolist()
            rest = "rest" if len(columns) > 3 else "stall"
            columns = [rest] + sorted(
                [column for column in columns if column != rest]
            )

        # Create matrix
        cm = numpy.zeros((len(columns), len(columns)))
        for i, col_i in enumerate(columns):
            for j, col_j in enumerate(columns):
                cm[i][j] = cm_table[
                    (cm_table["Actual"] == col_i)
                    & (cm_table["Predicted"] == col_j)
                ]["nPredictions"].iloc[0]

        cms.append(cm)

    return cms, columns


def plot_confusion_matrix(args):
    font = {"family": "serif"}
    is_da = "da" in args.group
    if not is_da:
        font["size"] = 14
    if is_da:
        plt.rc("font", **font)

    # Get data
    api = wandb.Api()
    runs = api.runs(
        "wulfdewolf/EMG-DAD-transformer",
        filters={"group": {"$in": [args.group]}},
    )
    cms = []
    classes = None
    for run in runs:
        cm, classes = get_cm(run, is_da)
        cms.append(cm)
    avg_cm = numpy.mean(numpy.stack(cms), axis=0)
    avg_cm = (avg_cm.swapaxes(0, 1) / avg_cm.sum(axis=2)).swapaxes(
        1, 0
    )  # average

    # Make confusion matrix plot
    names = (
        [
            "Thumb rotation",
            "Thumb flexion",
            "Index flexion",
            "Middle flexion",
            "Ring flexion",
            "Little flexion",
        ]
        if len(avg_cm) > 1
        else ["Movement"]
    )
    fig, axes = plt.subplots(
        figsize=(12, 6) if is_da else (10, 10),
        ncols=len(avg_cm),
        nrows=1,
        sharey=True,
        sharex=True,
    )
    axes = axes if is_da else [axes]
    for name, cm, ax in zip(names, avg_cm, axes):
        cm = cm / cm.sum(axis=0, keepdims=True)
        im = ax.imshow(
            cm,
            cmap="GnBu",
            vmin=0,
            vmax=1,
            extent=[0, len(classes), len(classes), 0],
            origin="upper",
        )

        # Add exact values
        for (j, i), label in numpy.ndenumerate(cm):
            ax.text(
                i + 0.5,
                j + 0.5,
                f"{round(label, 2)}",
                ha="center",
                va="center",
                # fontsize="x-small",
            )

        # Set labels/ticks/legend
        plt.yticks([i - 0.5 for i in range(1, len(classes) + 1)], classes)
        plt.xticks(
            [i - 0.5 for i in range(1, len(classes) + 1)],
            classes,
            ha="center" if is_da else "right",
            rotation=0 if is_da else 45,
        )
        ax.title.set_text(name)
    axes[0].set_ylabel("Predicted Class", weight="bold")
    fig.text(
        0.5, 0.27 if is_da else -0.05, "True Class", ha="center", weight="bold"
    )
    fig.colorbar(
        im,
        ax=axes,
        shrink=0.5,
        orientation="horizontal" if is_da else "vertical",
    )

    # Save
    if args.save is not None:
        plt.savefig(args.save, bbox_inches="tight")

    # Show
    plt.show()


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Transformer-Based Myoelectric Digit Action Decoding: confusion matrix"
    )
    parser.add_argument("group", type=str)
    parser.add_argument("--save", type=str, default=None)
    args = parser.parse_args()

    plot_confusion_matrix(args)
