import argparse
import os
import sys

import numpy
import randomname
import yaml

import wandb

# Add project root to path
sys.path.append(os.getcwd())

from src import utils
from src.config import Config
from src.dataset import EMGDataset
from src.evaluator import Evaluator


def script(args, config):
    # Create evaluator
    evaluator = Evaluator(
        None,
        config.dataset_config.gestures,
        config.dataset_config.digits,
        config.dataset_config.digit_actions,
    )

    # Loop over participants
    participants_results = []
    for participant in range(1, 1 + config.dataset_config.num_participants):
        # Initialise W&B
        run = wandb.init(
            project="EMG-DAD-transformer",
            group=f"{args.name}",
            name=f"partcipant {participant}",
            config=config,
            reinit=True,
            mode="online" if args.log_to_wandb else "disabled",
            dir=args.storage,
        )

        # Create dataset
        config.dataset_config.dataset_processed = "features"
        dataset = EMGDataset(
            args.storage,
            participant,
            features=True,
            selected_filter_orders=[],
            **config.dataset_config.dict(),
        )
        loaders = dataset.get_dataloaders()

        # Get baseline performance
        results = getattr(evaluator, f"evaluate_baseline_{args.task}")(loaders)

        # Log
        wandb.log(results)

        # Store
        participants_results.append(results)

        # End W&B run
        run.finish()

    # Calculate mean/median/... over participants
    results = {
        statistic: utils.calculate_statistic(statistic, participants_results)
        for statistic in ["mean", "median"]
    }

    # Print
    print("-" * 50 + f"\nRESULTS:\n")
    print(yaml.dump(results, allow_unicode=True, default_flow_style=False))


if __name__ == "__main__":
    # Read calling arguments
    parser = argparse.ArgumentParser(
        description="Transformer-Based Myoelectric Digit Action Decoding: baseline"
    )
    parser.add_argument("config", type=str)
    parser.add_argument("task", type=str, choices=["da_stall", "da_LDA", "stim_rest", "stim_LDA"])
    parser.add_argument("--log_to_wandb", action="store_true", default=False)
    parser.add_argument("--name", type=str, default=randomname.get_name())
    parser.add_argument("--storage", type=str, default=os.getcwd())
    args = parser.parse_args()

    # Read configuration
    config = Config(**utils.load_yaml(args.config))

    # Call script
    script(args, config)
