import argparse
import os
import sys

import randomname
import torch
import yaml

# Add project root to path
sys.path.append(os.getcwd())

from src import utils
from src.config import Config
from src.evaluator import Evaluator
from src.trainer import Trainer


def script(args, config):
    # Print experiment group name
    print("-" * 50 + f"\nNEW EXPERIMENT: {args.name}\n")

    # Set seed
    utils.setup_random_seed(config.training_config.random_seed)

    # Set device
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

    # Create evaluator
    evaluator = Evaluator(
        device,
        config.dataset_config.gestures,
        config.dataset_config.digits,
        config.dataset_config.digit_actions,
    )

    # Create trainer
    trainer = Trainer(
        evaluator=evaluator,
        device=device,
        config=config,
        log_to_wandb=args.log_to_wandb,
        storage=args.storage,
        mixed_precision=args.mixed_precision,
        compile=args.compile,
        optimising=False,
    )

    # Train and evaluate
    start_time = utils.start_timer()
    results, models = trainer.train(args.name)

    # Print final results
    print("-" * 50 + f"\nRESULTS:\n")
    utils.end_timer_and_print(start_time)
    print(yaml.dump(results, allow_unicode=True, default_flow_style=False))

    # Save models
    if args.save:
        # Prepare folder
        model_dir = os.path.join(args.storage, "models", args.name)
        if not os.path.exists(model_dir):
            os.makedirs(model_dir)

        # Save model per participant
        for participant, model_dict in enumerate(models, start=1):
            # Write summary
            with open(
                os.path.join(model_dir, f"participant_{participant}.txt"), "w"
            ) as f:
                f.write(str(model_dict["info"]))
            # Write weights
            torch.save(
                model_dict["model"].state_dict(),
                os.path.join(model_dir, f"participant_{participant}.pt"),
            )


if __name__ == "__main__":
    # Read calling arguments
    parser = argparse.ArgumentParser(
        description="Transformer-Based Myoelectric Digit Action Decoding: common training"
    )
    parser.add_argument("config", type=str)
    parser.add_argument("--log_to_wandb", action="store_true", default=False)
    parser.add_argument("--name", type=str, default=randomname.get_name())
    parser.add_argument("--storage", type=str, default=os.getcwd())
    parser.add_argument("--save", action="store_true", default=False)
    parser.add_argument(
        "--mixed_precision", action="store_true", default=False
    )
    parser.add_argument("--compile", action="store_true", default=False)
    args = parser.parse_args()

    # Read configuration
    config = Config(**utils.load_yaml(args.config))

    # Call script
    script(args, config)
