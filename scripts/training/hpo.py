import argparse
import functools
import os
import shutil
import sys

import optuna
import randomname
import ray.tune
import ray.tune.search.optuna
import torch
import yaml

# Add project root to path
sys.path.append(os.getcwd())

from src import utils
from src.config import Config, Hyperparams
from src.evaluator import Evaluator
from src.trainer import Trainer


def hpo_pp(run_folder, search_space):
    global_cwd = os.getcwd()

    # Trial function
    def train(participant, hyperparams):
        # Set seed for HPO trial
        utils.setup_random_seed(config.training_config.random_seed)

        # Get device
        device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

        # Create evaluator
        evaluator = Evaluator(
            device,
            config.dataset_config.gestures,
            config.dataset_config.digits,
            config.dataset_config.digit_actions,
        )

        # Parse hyperparameters
        config.hyperparams = [Hyperparams.parse_obj(hyperparams)]

        # Create trainer
        trainer = Trainer(
            evaluator,
            device,
            config,
            log_to_wandb=False,
            storage=args.storage,
            optimising=True,
            mixed_precision=args.mixed_precision,
            compile=args.compile,
            global_cwd=global_cwd,
        )

        # Train and evaluate
        results, _, _ = trainer.train_participant(participant, "")

        # Report results to tune
        ray.tune.report(
            **{
                config.training_config.hpo_metric[0]: results["validation"][
                    config.model_config.output_type
                ][config.training_config.hpo_metric[0]]["macro_average"]
            }
        )

    combined_best = {
        metric: [] for metric in config.training_config.hpo_metric
    }
    for participant in range(1, 1 + config.dataset_config.num_participants):
        participant_folder = os.path.join(
            run_folder, f"participant_{participant}"
        )

        # Restore existing run
        if os.path.exists(participant_folder):
            tuner = ray.tune.Tuner.restore(
                overwrite_trainable=ray.tune.with_resources(
                    functools.partial(train, participant),
                    {
                        "gpu": 1.0 / config.training_config.hpo_parallel,
                        "cpu": int(
                            (args.cpus - 1)
                            / config.training_config.hpo_parallel
                        ),
                    },
                ),
                path=participant_folder,
                resume_errored=True,
                resume_unfinished=True,
            )

        # Start a new one
        else:
            tuner = ray.tune.Tuner(
                ray.tune.with_resources(
                    functools.partial(train, participant),
                    {
                        "gpu": 1.0 / config.training_config.hpo_parallel,
                        "cpu": int(
                            (args.cpus - 1)
                            / config.training_config.hpo_parallel
                        ),
                    },
                ),
                tune_config=ray.tune.TuneConfig(
                    num_samples=config.training_config.hpo_samples,
                    search_alg=ray.tune.search.optuna.OptunaSearch(
                        search_space,
                        metric=config.training_config.hpo_metric,
                        mode=config.training_config.hpo_metric_mode,
                        seed=config.training_config.random_seed,
                    ),
                ),
                run_config=ray.air.config.RunConfig(
                    local_dir=os.path.join(args.storage, "hpo"),
                    name=os.path.join(
                        args.name,
                        f"participant_{participant}",
                    ),
                ),
            )

        # Run
        results = tuner.fit()

        # Extract best
        for metric, metric_mode in zip(
            config.training_config.hpo_metric,
            config.training_config.hpo_metric_mode,
        ):
            best = results.get_best_result(
                metric=metric, mode=metric_mode
            ).metrics
            print(best)
            combined_best[metric].append(best["config"])
            with open(
                os.path.join(
                    participant_folder, f"{metric_mode}_{metric}.yaml"
                ),
                "w",
            ) as f:
                f.write(
                    yaml.dump(
                        {
                            metric: float(best[metric]),
                            "hyperparams": best["config"],
                        },
                        allow_unicode=True,
                        default_flow_style=False,
                    )
                )

        # Delete trial folders
        for _, dirs, _ in os.walk(participant_folder):
            for dir in dirs:
                shutil.rmtree(os.path.join(participant_folder, dir))

    # Write combined best hyperparameters as config file
    config.hyperparams = combined_best
    with open(os.path.join(run_folder, f"best.yaml"), "w") as f:
        f.write(yaml.dump(config.dict()))


def hpo_common(run_folder, search_space):
    global_cwd = os.getcwd()

    # Trial function
    def train(
        hyperparams,
    ):
        # Set seed for HPO trial
        utils.setup_random_seed(config.training_config.random_seed)

        # Get device
        device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

        # Create evaluator
        evaluator = Evaluator(
            device,
            config.dataset_config.gestures,
            config.dataset_config.digits,
            config.dataset_config.digit_actions,
        )

        # Parse hyperparameters
        config.hyperparams = [Hyperparams.parse_obj(hyperparams)]

        # Create trainer
        trainer = Trainer(
            evaluator,
            device,
            config,
            log_to_wandb=False,
            storage=args.storage,
            optimising=True,
            mixed_precision=args.mixed_precision,
            compile=args.compile,
            global_cwd=global_cwd,
        )

        # Train and evaluate
        results, _ = trainer.train("")

        # Report results to tune
        ray.tune.report(
            **{
                config.training_config.hpo_metric[0]: results["median"][
                    "validation"
                ][config.model_config.output_type][
                    config.training_config.hpo_metric[0]
                ][
                    "macro_average"
                ]
            }
        )

    # Restore existing run
    if os.path.exists(os.path.join(run_folder, "tuner.pkl")):
        tuner = ray.tune.Tuner.restore(
            overwrite_trainable=ray.tune.with_resources(
                train,
                {
                    "gpu": 1.0 / config.training_config.hpo_parallel,
                    "cpu": int(
                        (args.cpus - 1) / config.training_config.hpo_parallel
                    ),
                },
            ),
            path=run_folder,
            resume_errored=True,
            resume_unfinished=True,
        )

    # Start new
    else:
        # RayTune HPO
        tuner = ray.tune.Tuner(
            ray.tune.with_resources(
                train,
                {
                    "gpu": 1.0 / config.training_config.hpo_parallel,
                    "cpu": int(
                        (args.cpus - 1) / config.training_config.hpo_parallel
                    ),
                },
            ),
            tune_config=ray.tune.TuneConfig(
                num_samples=config.training_config.hpo_samples,
                search_alg=ray.tune.search.optuna.OptunaSearch(
                    search_space,
                    metric=config.training_config.hpo_metric,
                    mode=config.training_config.hpo_metric_mode,
                    seed=config.training_config.random_seed,
                ),
            ),
            run_config=ray.air.config.RunConfig(
                local_dir=os.path.join(args.storage, "hpo"),
                name=args.name,
            ),
        )

    # Run
    results = tuner.fit()

    # Extract best
    for metric, metric_mode in zip(
        config.training_config.hpo_metric,
        config.training_config.hpo_metric_mode,
    ):
        with open(
            os.path.join(run_folder, f"{metric_mode}_{metric}.yaml"), "w"
        ) as f:
            best = results.get_best_result(
                metric=metric, mode=metric_mode
            ).metrics
            config.hyperparams = [best["config"]]
            f.write(
                yaml.dump(
                    config.dict(),
                    allow_unicode=True,
                    default_flow_style=False,
                )
            )

    # Delete trial folders
    for _, dirs, _ in os.walk(run_folder):
        for dir in dirs:
            shutil.rmtree(os.path.join(run_folder, dir))


def script(args, config):
    # Raytune settings
    os.environ[
        "TUNE_DISABLE_AUTO_CALLBACK_LOGGERS"
    ] = "1"  # Don't log anything through raytune --> better performance
    ray.init(num_cpus=args.cpus)

    # Set seed globally
    utils.setup_random_seed(config.training_config.random_seed)

    # Setup HPO run folder
    run_folder = os.path.join(
        args.storage,
        "hpo",
        args.name,
    )
    os.makedirs(run_folder, exist_ok=True)

    # Write config
    with open(os.path.join(run_folder, "config.yaml"), "a") as f:
        f.write(yaml.dump(config.dict()))

    # Establish hyperparameter search space
    search_space = {
        "batch_size": optuna.distributions.CategoricalDistribution(
            [32, 64, 128, 256, 512]
        ),
        "learning_rate": optuna.distributions.FloatDistribution(
            0.0001, 0.1, log=True
        ),
        "weight_decay": optuna.distributions.FloatDistribution(
            0.0001, 0.1, log=True
        ),
    }
    if config.model_config.pretrained_weights is None:
        search_space["dropout"] = optuna.distributions.FloatDistribution(
            0.0001, 0.1, log=True
        )
        search_space[
            "activation_function"
        ] = optuna.distributions.CategoricalDistribution(["relu", "gelu"])
        search_space["num_layers"] = optuna.distributions.IntDistribution(
            1, 10
        )
        if config.model_config.net != "FTR":
            search_space[
                "input_dim"
            ] = optuna.distributions.CategoricalDistribution(
                [64, 128, 256, 512]
            )
            search_space[
                "num_heads"
            ] = optuna.distributions.CategoricalDistribution([2, 4, 8, 16])

    if config.training_config.hpo_pp:
        hpo_pp(run_folder, search_space)
    else:
        hpo_common(run_folder, search_space)


if __name__ == "__main__":
    # Read calling arguments
    parser = argparse.ArgumentParser(
        description="Transformer-Based Myoelectric Digit Action Decoding: common hyperparameter optimisation"
    )
    parser.add_argument(
        "config",
        type=str,
        default="configs/hpo.yaml",
    )
    parser.add_argument(
        "--cpus",
        type=int,
        default=16,
    )
    parser.add_argument(
        "--name",
        type=str,
        default=randomname.get_name(),
    )
    parser.add_argument(
        "--storage",
        type=str,
        default=os.getcwd(),
    )
    parser.add_argument(
        "--mixed_precision", action="store_true", default=False
    )
    parser.add_argument("--compile", action="store_true", default=False)
    args = parser.parse_args()

    # Read configuration
    config = Config(**utils.load_yaml(args.config))

    # Call script
    script(args, config)
