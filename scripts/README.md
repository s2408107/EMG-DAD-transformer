# Scripts

```bash
analysis
├── confusion_matrix.py
├── evaluate_metrics.py
├── metrics.py
├── PCA.py
└── position.py
csd3
├── hpo.sh
└── setup_env.sh
data
├── download.py
├── plot.py
└── process.py
training
├── baseline.py
├── hpo.py
└── train.py
```

## 1. Analysis
The scripts in [analysis](analysis/) can be used to calculate/plot the various metrics I report in my dissertation.  
Some of these require results to be on [Weights&Biases](www.wandb.ai) (through [`train.py`](/scripts/traiing/train.py)'s `--log_to_wandb` argument).

## 2. CSD3
The scripts in [csd3](csd3/) are the scripts I used to run evaluations on [Cambridge's CDS3 HPC](https://docs.hpc.cam.ac.uk/hpc/).  
Unless you are running experiments on the same HPC, these scripts will not be useful to you.

## 3. Data
The scripts in [data](data/) can be used to download, plot, and process the data.  
The [`process.py`](data/process.py) script requires a [configuration file](/configs/).

## 4. Training
The scripts in [training](training/) can be used to run the baseline models, perform HPO, and train the transformer models.  
All of these scripts require a [configuration file](/configs/).