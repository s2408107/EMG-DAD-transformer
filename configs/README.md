# Configuration files

```bash
bests
├── da
│   ├── features
│   ├── fnet
│   ├── full
│   └── tnet
└── stim
hpo
├── da
│   ├── features
│   ├── fnet
│   ├── full
│   └── tnet
└── stim
```

The configuration files in [bests](bests/) are the results of running the [HPO script](/scripts/training/hpo.py) with the configuration files in [hpo](hpo/).  
The evaluations from my dissertation can be repeated by running the [`train.py`](/scripts/training/train.py) with a configuration file from [bests](bests/).  
Note that to train a FULL model, its TNET and FNET parts need to be trained and saved (through [`train.py`](/scripts/traiing/train.py)'s `--save` argument) first.
